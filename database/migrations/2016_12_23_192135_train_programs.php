<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrainPrograms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('train_programs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sex'); 
            $table->float('age_start', 8, 2);
            $table->float('age_end', 8, 2);
            $table->string('imt_type');
            $table->integer('imt_start');
            $table->integer('imt_end');
            $table->string('target');
            $table->string('week_amount');
            $table->string('cardio');
            $table->string('strong');
            $table->integer('serial_number');
            $table->string('train_num_week');
            $table->string('train_duration');
            $table->string('place');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('train_programs');
    }
}
