<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Accessories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('accessories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('simple_exercise_id')->unsigned()->index();
            $table->foreign('simple_exercise_id')->references('id')->on('simple_exercises')->onDelete('cascade');
            $table->string('accessory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accessories');
    }
}
