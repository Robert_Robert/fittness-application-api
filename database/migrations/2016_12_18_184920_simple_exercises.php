<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimpleExercises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('simple_exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('image')->nullable();
            $table->string('description', 2000);
            $table->string('muscle_group');
            $table->string('animation')->nullable();
            $table->string('exercise_type');
            $table->string('always_injury');
            $table->string('newer_injury');
             $table->string('weight');
             $table->string('user_level');
             $table->integer('exercise_level');
             $table->string('place');
             $table->string('sex');
            $table->integer('hidden_exercise');
            $table->string('load_type');
            $table->date('date');   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('simple_exercises');
    }
}
