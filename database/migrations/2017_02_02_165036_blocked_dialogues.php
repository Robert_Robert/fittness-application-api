<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlockedDialogues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocked_dialogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dialog_id')->unsigned()->index();
            $table->foreign('dialog_id')->references('id')->on('dialogs')->onDelete('cascade');
            $table->integer('blocked_user')->unsigned()->index();
            $table->foreign('blocked_user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blocked_dialogs');
    }
}
