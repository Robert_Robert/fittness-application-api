<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrainExercises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('train_exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('train_day_id')->unsigned()->index();
            $table->foreign('train_day_id')->references('id')->on('train_days')->onDelete('cascade');
            $table->integer('simple_exercise_id')->unsigned()->index();
            $table->foreign('simple_exercise_id')->references('id')->on('simple_exercises')->onDelete('cascade');
            $table->string('attempt_numb');
            $table->string('weight');
            $table->string('repeat_numb');
            $table->string('relax_b_repeat');
            $table->string('relax_b_exercise');


            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('train_exercises');
    }
}
