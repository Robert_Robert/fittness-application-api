<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrainDays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('train_days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('train_program_id')->unsigned()->index();
            $table->foreign('train_program_id')->references('id')->on('train_programs')->onDelete('cascade');
            $table->string('name');
            $table->string('image')->nullable();
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('train_days');
    }
}
