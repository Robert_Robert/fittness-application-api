<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/test-socket', function(){
//    return view('test-socket');
    event(
        new \App\Events\TestEvent()
    );
});

Route::get('/admin-login', 'Auth\LoginController@loginForm');

Route::post('/admin-login', 'Auth\LoginController@adminLogin');
Route::get('/admin-logout', 'Auth\LoginController@adminLogout');

Route::group(['middleware' => 'login'], function() {
    Route::get('/', 'AdminController@index');
    Route::get('/new-admin', 'Auth\RegisterController@newAdmin');
    Route::post('/new-admin', 'Auth\RegisterController@addNewAdmin');
   


    Route::get('/exercise-list', 'AdminController@exercise_list');
    Route::get('/program-list', 'AdminController@program_list');


    Route::get('/add-simple-exercise', function () {
        $groups = App\Models\Simple_exercise::select('muscle_group')->distinct()->get();
        return view('dashboard.add_simple_exercise', ['groups' => $groups]);
    });

    Route::post('/add-new-exercise', 'AdminController@add_new_exercise');

    Route::get('/edit-simple-exercise/{exercise}', 'AdminController@exercise_edit_form');

    Route::delete('/del-accessory/{model}', 'AdminController@delete_accessory');
    Route::delete('/del-exercise/{model}', 'AdminController@delete_exercise');
    Route::put('/edit-exercise/{model}', 'AdminController@edit_exercise');

    Route::get('/search-exercises', 'AdminController@search_exercises');
    Route::get('/search-program', 'AdminController@search_programs');


    Route::get('/add-new-program', 'AdminController@new_program_form');
    Route::post('/add-program', 'AdminController@add_new_program');
    Route::get('/edit-program/{program}', 'AdminController@show_program');
    Route::post('/edit-program/{program}', 'AdminController@edit_program');
    Route::post('/add-train-day/{program}', 'AdminController@add_train_day');

    Route::post('/edit-train-day/{program}/{day}', 'AdminController@edit_train_day');
    Route::delete('/del-day/{day}', 'AdminController@del_day');

});

//Route::group(['domain' => 'api.foodand.fit'], function () {
#API
Route::group(['middleware' => ['api_auth']], function () {
# Dialogue routes
    Route::post('/create-dialog', 'DialogController@createDialog');
    Route::delete('/delete-dialog', 'DialogController@deleteteDialog');
    Route::post('/add-message', 'DialogController@addMessage');
    Route::post('/add-file', 'DialogController@addFile');
    Route::get('/get-history', 'DialogController@getHistory');
    Route::post('/block-dialog', 'DialogController@blockDialog');
    Route::get('/get-dialog-list', 'DialogController@getList');

# Feed routes
   // Route::get('/get-subscribers', 'PublicationController@getSubscribers')->middleware('api_auth');
    Route::get('/get-subscribers', 'PublicationController@getSubscribers');
    Route::get('/get-subscribes', 'PublicationController@getSubscribes');
    Route::post('/add-publication', 'PublicationController@addPublication');
    Route::get('/search-users', 'PublicationController@searchUsers');
    Route::get('/search-publication', 'PublicationController@searchPublication');
    Route::post('/edit-publication', 'PublicationController@editPublication');
    Route::post('/make-subscribe', 'PublicationController@makeSubcribe');
    Route::delete('/make-unsubscribe', 'PublicationController@makeUnSubcribe');
    Route::get('/get-user-publication', 'PublicationController@getUserPublication');
    Route::delete('/delete-publication', 'PublicationController@deletePublication');
    Route::post('/like-publication', 'PublicationController@likePub');
    Route::post('/dislike-publication', 'PublicationController@disLikePub');
    Route::get('/get-subscribe-pub', 'PublicationController@getSubscribePub');
    Route::get('/get-publication-comments', 'PublicationController@getComments');
    Route::get('/publication-attach-users', 'PublicationController@getAttachUsers');
    Route::get('/publication-by-id', 'PublicationController@getPublicationBuId');
    Route::post('/leave-comment', 'PublicationController@leaveComment');
    Route::put('/edit-comment', 'PublicationController@editComment');
    Route::delete('/delete-comment', 'PublicationController@deleteComment');
    Route::post('/complaint-publication', 'PublicationController@complaintPublication');


#Hadle authorization


    Route::post('/attach-device-token', 'Auth\LoginController@attachDeviceToken');



#PROFILE
    Route::get('/get-profile', 'ProfileController@getProfileById');
    Route::post('/edit-profile', 'ProfileController@editProfile');
    Route::put('/change-password', 'ProfileController@changePassword');
    Route::put('/addbadge', 'DialogController@badge');
});
#Hadle authorization
Route::post('/login-user', 'Auth\LoginController@handleLogin');
Route::get('/check-user', 'Auth\LoginController@checkUser');
Route::get('/restoration-password', 'Auth\LoginController@restorePassword');
Route::delete('/logout-user', 'Auth\LoginController@handleLogout');
#Handle Registration
Route::post('/registration-user', 'Auth\RegisterController@userRegistration');
//
//});

Auth::routes();

Route::get('/home', 'HomeController@index');
