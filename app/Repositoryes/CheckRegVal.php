<?php

namespace App\Repositoryes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class CheckRegVal
{

    public function check(Request $request){
        if(isset($request->point)){
            $point = $request->point;
            if($point !== "male" && $point !== "female") return response()->json(['status' => 'error', 'error' => ['code' => 26, 'desc' => 'Invalid point value']]);

        }

        if(isset($request->physical_level)){
            $level = $request->physical_level;
            if($level != "hard" && $level != "medium" && $level != "easy") return response()->json(['status' => 'error', 'error' => ['code' => 27, 'desc' => 'Invalid physical_level value']]);
        }

        if(isset($request->train_place)){
            $place = $request->train_place;
            if($place != "home" && $place != "gym") return response()->json(['status' => 'error', 'error' => ['code' => 28, 'desc' => 'Invalid train_place value']]);
        }

        if(isset($request->target)){
            $target = $request->target;
            $target_array = ['slim', 'set_weight', 'relax', 'legs', 'back', 'restoration', 'set_muscles'];
            $search = array_search($target, $target_array);
            if($search === false) return response()->json(['status' => 'error', 'error' => ['code' => 29, 'desc' => 'Invalid target value']]);
        }

        if(isset($request->train_time)){
            $time = $request->train_time;
            if($time != "25" && $time != "55") return response()->json(['status' => 'error', 'error' => ['code' => 30, 'desc' => 'Invalid train_time value']]);
        }

        return true;
    }




}