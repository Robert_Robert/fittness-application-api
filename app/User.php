<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Subscribe;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'reserve_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function subscribers(){
        return $this->hasMany('App\Models\Subscribe', 'cur_user_id');
    }

    public function subscribes(){
        return $this->hasMany('App\Models\Subscribe', 'subscriber_id');
    }

    public function userNote(){
        return $this->hasOne('App\Models\UserNote', 'user_id');
    }

    public function userMedicalNote(){
        return $this->hasOne('App\Models\UserMedicalNotes', 'user_id');
    }

    public function userPhysicalNote(){
        return $this->hasOne('App\Models\UserPhysicalNote', 'user_id');
    }

    public function userTrainRecomendation(){
        return $this->hasOne('App\Models\UserTrainRecomendation', 'user_id');
    }

    public function userFoodBehaviorRec(){
        return $this->hasOne('App\Models\UserFoodBehaviorRec', 'user_id');
    }
    
    
}
