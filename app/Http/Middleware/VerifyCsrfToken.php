<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/create-dialog',
        '/delete-dialog',
        '/add-message',
        '/add-file',
        '/get-history',
        '/block-dialog',
        '/get-dialog-list',
        '/get-subscribers',
        '/get-subscribes',
        '/add-publication',
        '/search-users',
        '/edit-publication',
        'search-publication',
        '/make-subscribe',
        '/make-unsubscribe',
        '/get-user-publication',
        '/delete-publication',
        '/like-publication',
        '/dislike-publication',
        '/get-subscribe-pub',
        '/get-publication-comments',
        '/publication-attach-users',
        '/publication-by-id',
        '/leave-comment',
        '/edit-comment',
        '/delete-comment',
        '/complaint-publication',
        '/registration-user',
        '/login-user',
        '/logout-user',
        '/check-user',
        '/restoration-password',
        '/attach-device-token',
        '/get-profile',
        '/edit-profile',
        '/change-password',
        '/addbadge'
    ];
}
