<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($request->user_token)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>26,
                'desc'=>'missing user token'
            ]]);
        }elseif(!isset($request->auth_user_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>35,
                'desc'=>'empty auth user value'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->auth_user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'desc":"user is not found'
            ]]);
        }elseif(empty(DB::table('users_tokens')->where('token', '=', $request->user_token)->first())){
                return response()->json(['status'=>'error', 'error'=>[
                    'code'=>27,
                    'desc'=>'invalid user token'
                ]]);
        }else{
            $tokenANDuser = DB::table('users_tokens')->where('user_id', '=', $request->auth_user_id)->where('token', '=', $request->user_token)->first();
            if(empty($tokenANDuser)){
                return response()->json(['status'=>'error', 'error'=>[
                    'code'=>36,
                    'desc'=>'token behave for other user'
                ]]);
            }
        }


        return $next($request);
    }
}
