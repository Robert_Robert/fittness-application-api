<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\AdminUser;
class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = new AdminUser();
        if (!$request->session()->has('auth')) {
            return redirect('/admin-login');
        }
        if(!$user->checkHash($request->session()->get('auth'))){
            $request->session()->forget('auth');
            return redirect('/admin-login');
        }

        return $next($request);
    }
}
