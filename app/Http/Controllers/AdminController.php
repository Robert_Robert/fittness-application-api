<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Simple_exercise;
use App\Models\Accessory;
use Validator;
use App\Models\TrainProgram;
use App\Models\TrainDay;
use App\Models\TrainExercise;

class AdminController extends Controller
{

	public function index(Request $request){
		return redirect('/exercise-list');
		//return view('dashboard.layout');
	}

	public function exercise_list(Simple_exercise $exercise){
		$exercises = $exercise->paginate(5);
		$groups = $exercise->select('muscle_group')->distinct()->get();
		return view('dashboard.exercises_list', ['exercises'=>$exercises, 'groups'=>$groups]);
	}

	public function program_list(TrainProgram $program){
		$programs = $program->all();
		return view('dashboard.programs_list', ['programs'=>$programs]);
	}

    public function add_new_exercise(Request $request, Simple_exercise $exercise){

    		$validator = Validator::make(
		 array(
			   'Название'	=> $request->name,
			   'Описание'	=> $request->description,
			   'Група_мышц'	=> $request->muscle_group,
			   'Тип_упражнения'	=> $request->exercise_type,

			 ),
		 array(
			   'Название' => 'required',
			   'Описание' => 'required',
			   'Група_мышц' => 'required',
			   'Тип_упражнения' => 'required',

			 ));

			 if ($validator->fails()) {
 					return redirect()
 								->back()
 											->withErrors($validator)
 											->withInput();
 			}


    	$exercise->add_new_execrise($request);

    	return redirect('exercise-list')->with('message', 'Упражнение добавлено');

    
    }

    public function exercise_edit_form(Simple_exercise $exercise){
		$groups = Simple_exercise::select('muscle_group')->distinct()->get();
    	return view('dashboard.edit_exercise_form', ['exercise'=>$exercise, 'accessories'=>$accessory = $exercise->accessories()->get(), 'groups'=>$groups]);
    }

    public function delete_accessory(Accessory $model){
    	$model->delete();
    	return 1;
    }

    public function delete_exercise(Simple_exercise $model){
    	$model->delete_exercise();
    	return 1;	
    }

    public function edit_exercise(Simple_exercise $model, Request $request){
    	$validator = Validator::make(
		 array(
			   'Название'	=> $request->name,
			   'Описание'	=> $request->description,
			   'Група_мышц'	=> $request->muscle_group,
			   'Тип_упражнения'	=> $request->exercise_type,

			 ),
		 array(
			   'Название' => 'required',
			   'Описание' => 'required',
			   'Група_мышц' => 'required',
			   'Тип_упражнения' => 'required',

			 ));

			 if ($validator->fails()) {
 					return redirect()
 								->back()
 											->withErrors($validator)
 											->withInput();
 			}

 		$model->edit_execrise($request);

    	return redirect('exercise-list')->with('message', 'Упражнение изменено');	
    }

    public function search_exercises(Request $request, Simple_exercise $exercise){
    	$exercises = $exercise->serchExercise($request); 
    	$groups = $exercise->select('muscle_group')->distinct()->get();

    	return view('dashboard.search_exercises', ['exercises'=>$exercises, 'groups'=>$groups]);
    }

    public function search_programs(Request $request, TrainProgram $program){


    	$programs = $program->search($request);
    	return view('dashboard.programs_list', ['programs'=>$programs]);
    }

    public function new_program_form(Simple_exercise $exercise){
    	$exercises = $exercise->all();
    	return view('dashboard.program_form', ['exercises'=>$exercises]);
    }

    public function add_new_program(TrainProgram $program, Request $request){


//    	$checkProgram = TrainProgram::where('sex', '=', $request->sex)->where('age_group', '=', $request->age_group)->where('imt', '=', $request->imt)->where('target', '=', $request->target)->where('train_num_week', '=', $request->train_num_week)->where('train_duration', '=', $request->train_duration)->first();
//
//
//
//    	if(!empty($checkProgram)){
//    		return redirect()->back()->with('notice', 'Програма не добавлена! Така комбинация уже существует.');
//    	}

		isset($request->cardio) ? $cardio = $request->cardio : $cardio = 0;
		isset($request->strong) ? $strong = $request->strong : $strong = 0;

		$age_array = [];
		if($request->age_group == '18-45'){
			$age_array[0] = 18;
			$age_array[1] = 45;
		}elseif($request->age_group == '45-70'){
			$age_array[0] = 45;
			$age_array[1] = 70;
		}else {
			$age_array[0] = 0;
			$age_array[1] = 0;
		}
		
		$imt_array = [];
		switch ($request->imt){
			case '15-21.9':
				$imt_array[0] = 15;
				$imt_array[1] = 21.9;
				break;
			case '22-26':
				$imt_array[0] = 22;
				$imt_array[1] = 26;
				break;
			case '27-30':
				$imt_array[0] = 27;
				$imt_array[1] = 30;
				break;
			case '31-100':
				$imt_array[0] = 31;
				$imt_array[1] = 100;
				break;
		}
    	$program_id = $program->insertGetId([
    								'sex' => $request->sex,
    								'age_start' => $age_array[0],
									'age_end'   => $age_array[1],
									'imt_type'  => $request->imt_type,
    								'imt_start' => $imt_array[0],
									'imt_end'   => $imt_array[1],
    								'target' => $request->target,
    								'train_num_week' => $request->train_num_week,
									'week_amount'    => $request->week_amount,
									'serial_number'  => $request->serial_number,
									'cardio'         => $cardio,
									'strong'         => $strong,
    								'train_duration' => $request->train_duration,
									'place'          => $request->place
   		]);

   		return redirect('edit-program/'.$program_id);

    }

    public function show_program(TrainProgram $program, Simple_exercise $exercise){
    	$counter = 0;
    	$train_days = [];
    	$exercises = $exercise->all();
    	$train = $program->days()->get();

    	foreach ($train as $train_item) {
    		$train_days[$counter]['id'] = $train_item->id;
    		$train_days[$counter]['name'] = $train_item->name;
			$train_days[$counter]['image'] = $train_item->image;
    		$train_days[$counter]['exercises'] = $train_item->trainExercises()->get();
    		$counter++;
    	}


    	return view('dashboard.program_form', ['exercises'=>$exercises, 'train_days'=>$train_days, 'program'=>$program]);

    	}

    public function edit_program(TrainProgram $program, Request $request){
		$age_array = [];
		if($request->age_group == '18-45'){
			$age_array[0] = 18;
			$age_array[1] = 45;
		}elseif($request->age_group == '45-70'){
			$age_array[0] = 45;
			$age_array[1] = 70;
		}else {
			$age_array[0] = 0;
			$age_array[1] = 0;
		}

		$imt_array = [];
		switch ($request->imt){
			case '15-21.9':
				$imt_array[0] = 15;
				$imt_array[1] = 21.9;
				
				break;
			case '22-26':
				$imt_array[0] = 22;
				$imt_array[1] = 26;
			
				break;
			case '27-30':
				$imt_array[0] = 27;
				$imt_array[1] = 30;
				
				break;
			case '31-100':
				$imt_array[0] = 31;
				$imt_array[1] = 100;
				
				break;
		}


    		$program->sex = $request->sex;
    		$program->age_start = $age_array[0];
		    $program->age_end = $age_array[1];
			$program->imt_type = $request->imt_type;
    		$program->imt_start = $imt_array[0];
			$program->imt_end = $imt_array[1];
    		$program->target = $request->target;
			$program->week_amount = $request->week_amount;
		    isset($request->cardio) ? $program->cardio = $request->cardio : $program->cardio = 0;
		    isset($request->strong) ? $program->strong = $request->strong : $program->strong = 0;
			$program->train_num_week = $request->train_num_week;
		    $program->serial_number = $request->serial_number;
    		$program->train_duration = $request->train_duration;
			$program->place = $request->place;
    		$program->save();
    	return redirect('edit-program/'.$program->id);	
    }	

    public function add_train_day(TrainProgram $program, Request $request, TrainDay $day){
    	$day->addDay($request, $program);
    	return redirect('edit-program/'.$program->id);
    }

    public function edit_train_day(TrainProgram $program, TrainDay $day, Request $request){
    	$day->editDay($request, $program);
    	return redirect('edit-program/'.$program->id);
    }	

    public function del_day(TrainDay $day){
    	$day->delete();
    	return 1;
    }
}
