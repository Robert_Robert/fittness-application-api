<?php

namespace App\Http\Controllers\Auth;

use App\Models\AdminUser;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PushNotification;
use Hash;
use App\Repositoryes\CheckRegVal;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
//            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function userRegistration(Request $request, CheckRegVal $check)
    {
        if (!isset($request->email) || !isset($request->name) || !isset($request->surname) || !isset($request->birthDate)  || !isset($request->point) || !isset($request->target)) {
            return response()->json(['status' => 'error', 'error' => [
                'code' => 8,
                'desc' => 'check parametrs list and their fill'
            ]]);
        } elseif (!empty(DB::table('users')->where('email', '=', $request->email)->first())) {
            return response()->json(['status' => 'error', 'error' => [
                'code' => 21,
                'desc' => 'E-mail already busy'
            ]]);
        } else {
            $valid = $check->check($request);
            if($valid !== true) return $valid;

            if(isset($request->password)) {
                $name = $request->name . ' ' . $request->surname;
                $reserve_password = substr(md5(Hash::make($request->password)), -10, 10);
                $user_id = DB::table('users')->insertGetId(['name' => $name,
                    'email' => $request->email,
                    'password' => md5($request->password),
                    'reserve_password'=>$reserve_password
                ]);
                $city = isset($request->city) ? $request->city : null;

                DB::table('user_notes')->insert(['name' => $name,
                    'age' => $request->birthDate,
                    'sex' => $request->point,
                    'city' => $city,
                    'target' => $request->target,
                    'user_id' => $user_id]);

                if(isset($request->avatar)) {
                    if(gettype($request->avatar) == 'string'){
                        DB::table('user_notes')->where('user_id', '=', $user_id)->update(['avatar'=>$request->avatar]);
                    }else {
                        $filename = uniqid('avatar') . '.' . $request->avatar->getClientOriginalExtension();
                        $path = $request->avatar->move('public/users_files', $filename);
                        DB::table('user_notes')->where('user_id', '=', $user_id)->update(['avatar' => 'http://' . $_SERVER['SERVER_NAME'] . '/' . (string)$path]);
                    }
                }

                if (isset($request->device_token)) {
                    DB::table('device_tokens')->insert(['user_id' => $user_id, 'token' => $request->device_token]);
                }
                $userToken = password_hash($request->birthDate . $name . $request->city, PASSWORD_DEFAULT);
                DB::table('users_tokens')->insert(['user_id'=>$user_id, 'token'=>$userToken]);

                $weight = $request->weight ? $request->weight : null;
                $height = $request->height ? $request->height : null;
                $physical_level = $request->physical_level ? $request->physical_level : null;

                DB::table('user_physical_notes')->insert(['weight'=>$weight, 'height'=>$height, 'physical_level'=>$physical_level, 'user_id'=>$user_id]);

                $train_place = $request->train_place ? $request->train_place : null;
                $train_time = $request->train_time ? $request->train_time : null;
                DB::table('user_train_recomendation')->insert(['train_duration'=>$train_time, 'place'=>$train_place, 'user_id'=>$user_id]);

                $injury = $request->injury ? $request->injury : null;
                DB::table('injuries')->insert(['injury'=>$injury, 'user_id'=>$user_id]);
                $disease = $request->disease ? $request->disease : null;
                DB::table('diseases')->insert(['disease'=>$disease, 'user_id'=>$user_id]);
                $allergy = $request->allergy ? $request->allergy : null;
                DB::table('allergies')->insert(['alergy'=>$allergy, 'user_id'=>$user_id]);

                return response()->json(['status' => 'succes', 'user_id' => $user_id, 'user_token'=>$userToken]);
            }elseif(isset($request->fb_token)){
                if(!isset($request->fb_id)) return response()->json(['status' => 'error', 'error' => ['code' => 30, 'desc' => 'missing fb_id']]);
                $name = $request->name . ' ' . $request->surname;
                $user_id = DB::table('users')->insertGetId(['name' => $name,
                    'email' => $request->email,
                    'password' => null]);
                $city = isset($request->city) ? $request->city : null;

                DB::table('user_notes')->insert(['name' => $name,
                    'age' => $request->birthDate,
                    'sex' => $request->point,
                    'city' => $city,
                    'target' => $request->target,
                    'user_id' => $user_id]);

                if(isset($request->avatar)) {
                    if(gettype($request->avatar) == 'string'){
                        DB::table('user_notes')->where('user_id', '=', $user_id)->update(['avatar'=>$request->avatar]);
                    }else {
                        $filename = uniqid('avatar') . '.' . $request->avatar->getClientOriginalExtension();
                        $path = $request->avatar->move('public/users_files', $filename);
                        DB::table('user_notes')->where('user_id', '=', $user_id)->update(['avatar' => 'http://' . $_SERVER['SERVER_NAME'] . '/' . (string)$path]);
                    }
                }

                if (isset($request->device_token)) {
                    DB::table('device_tokens')->insert(['user_id' => $user_id, 'token' => $request->device_token]);
                }
                $userToken = password_hash($request->birthDate . $name . $request->city, PASSWORD_DEFAULT);
                DB::table('users_tokens')->insert(['user_id'=>$user_id, 'token'=>$userToken]);

                $weight = $request->weight ? $request->weight : null;
                $height = $request->height ? $request->height : null;
                $physical_level = $request->physical_level ? $request->physical_level : null;

                DB::table('user_physical_notes')->insert(['weight'=>$weight, 'height'=>$height, 'physical_level'=>$physical_level, 'user_id'=>$user_id]);

                $train_place = $request->train_place ? $request->train_place : null;
                $train_time = $request->train_time ? $request->train_time : null;
                DB::table('user_train_recomendation')->insert(['train_duration'=>$train_time, 'place'=>$train_place, 'user_id'=>$user_id]);

                $injury = $request->injury ? $request->injury : null;
                DB::table('injuries')->insert(['injury'=>$injury, 'user_id'=>$user_id]);
                $disease = $request->disease ? $request->disease : null;
                DB::table('diseases')->insert(['disease'=>$disease, 'user_id'=>$user_id]);
                $allergy = $request->allergy ? $request->allergy : null;
                DB::table('allergies')->insert(['alergy'=>$allergy, 'user_id'=>$user_id]);



                DB::table('facebook_tokens')->insert(['user_id'=>$user_id, 'token'=>$request->fb_token, 'fb_id'=>$request->fb_id]);

                return response()->json(['status' => 'succes', 'user_id' => $user_id, 'user_token'=>$userToken]);
                
                
            }elseif(isset($request->vk_token)){
                if(!isset($request->vk_id)) return response()->json(['status' => 'error', 'error' => ['code' =>31, 'desc' => 'missing vk_id']]);
                $name = $request->name . ' ' . $request->surname;
                $user_id = DB::table('users')->insertGetId(['name' => $name,
                    'email' => $request->email,
                    'password' => null]);
                $city = isset($request->city) ? $request->city : null;

                DB::table('user_notes')->insert(['name' => $name,
                    'age' => $request->birthDate,
                    'sex' => $request->point,
                    'city' => $city,
                    'target' => $request->target,
                    'user_id' => $user_id]);

                if(isset($request->avatar)) {
                    if(gettype($request->avatar) == 'string'){
                        DB::table('user_notes')->where('user_id', '=', $user_id)->update(['avatar'=>$request->avatar]);
                    }else {
                        $filename = uniqid('avatar') . '.' . $request->avatar->getClientOriginalExtension();
                        $path = $request->avatar->move('public/users_files', $filename);
                        DB::table('user_notes')->where('user_id', '=', $user_id)->update(['avatar' => 'http://' . $_SERVER['SERVER_NAME'] . '/' . (string)$path]);
                    }
                }

                if (isset($request->device_token)) {
                    DB::table('device_tokens')->insert(['user_id' => $user_id, 'token' => $request->device_token]);
                }
                $userToken = password_hash($request->birthDate . $name . $request->city, PASSWORD_DEFAULT);
                DB::table('users_tokens')->insert(['user_id'=>$user_id, 'token'=>$userToken]);

                $weight = $request->weight ? $request->weight : null;
                $height = $request->height ? $request->height : null;
                $physical_level = $request->physical_level ? $request->physical_level : null;

                DB::table('user_physical_notes')->insert(['weight'=>$weight, 'height'=>$height, 'physical_level'=>$physical_level, 'user_id'=>$user_id]);

                $train_place = $request->train_place ? $request->train_place : null;
                $train_time = $request->train_time ? $request->train_time : null;
                DB::table('user_train_recomendation')->insert(['train_duration'=>$train_time, 'place'=>$train_place, 'user_id'=>$user_id]);

                $injury = $request->injury ? $request->injury : null;
                DB::table('injuries')->insert(['injury'=>$injury, 'user_id'=>$user_id]);
                $disease = $request->disease ? $request->disease : null;
                DB::table('diseases')->insert(['disease'=>$disease, 'user_id'=>$user_id]);
                $allergy = $request->allergy ? $request->allergy : null;
                DB::table('allergies')->insert(['alergy'=>$allergy, 'user_id'=>$user_id]);

                DB::table('vkontakte_tokens')->insert(['user_id'=>$user_id, 'token'=>$request->vk_token, 'vk_id'=>$request->vk_id]);

                return response()->json(['status' => 'succes', 'user_id' => $user_id, 'user_token'=>$userToken]);
            }else{
                return response()->json(['status' => 'error', 'error' => [
                    'code' => 8,
                    'desc' => 'check parametrs list and their fill'
                ]]);
            }

        }
    }
    
    public function newAdmin(){
        return view('dashboard.registration');
    }
    
    public function addNewAdmin(Request $request, AdminUser $user){
        $validator = Validator::make(
            array('Логин' => $request->login,
                'Пароль'	=> $request->password,
                'Повторный_пароль' => $request->repeat_password
            ),
            array('Логин' => 'required|email',
                'Пароль' => 'required',
                'Повторный_пароль' => 'required'
            ));
        $validator->after(function ($validator) use ($request, $user) {
            if ($request->password != $request->repeat_password) {
                $validator->errors()->add('Пароли', 'Пароли не совпадают!');
            }
            if($user->checkMail($request->login) == true){
                $validator->errors()->add('Занято', 'Администратор с таким email уже существует!');
            }
        });
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user_pass_hash = $user->addNewAdmin($request->login, $request->password);
//        $request->session()->put('auth', $user_pass_hash);
        return redirect()->back()->with('message', 'Добавлен новый администратор');


    }
}