<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Http\Response;
use PushNotification;
use Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\AdminUser;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function handleLogin(Request $request)
    {
        if (!isset($request->email)) {
            return response()->json(['status' => 'error', 'error' => [
                'code' => 8,
                'desc' => 'check parametrs list and their fill'
            ]]);
        } else {
//            $user = DB::table('users')->where('password', '=', md5($request->password))->where('email', '=', $request->email)->first();
            $user = DB::table('users')->where('email', '=', $request->email)->first();


            if (empty($user)) {
                return response()->json(['status'=>'error', 'error'=>[
                    'code'=>24,
                    'desc'=>'mail is not found'
                ]]);
            }elseif(isset($request->password)){
                if($user->password != md5($request->password)){
                    if($user->reserve_password !== $request->password) {
                        return response()->json(['status' => 'error', 'error' => ['code' => 23, 'desc' => 'invalid password']]);
                    }else{
                        $this->changePass($request, $user);
                    }
                }
                $token = DB::table('device_tokens')->where('user_id', '=', $user->id)->where('token', '=', $request->device_token)->first();
                if (empty($token) && isset($request->device_token)){
                    DB::table('device_tokens')->insert(['user_id' => $user->id, 'token' => $request->device_token]);
                }
                $user_info = DB::table('user_notes')->where('user_id', '=', $user->id)->first();
                $user_token = password_hash($user_info->age . $user_info->name . $user_info->city, PASSWORD_DEFAULT);
                DB::table('users_tokens')->insert(['user_id'=>$user->id, 'token'=>$user_token]);

                return response()->json(['status' => 'succes', 'user_id' => $user->id, 'user_token'=>$user_token]);
            }elseif(isset($request->fb_token)){
                $fb_token = DB::table('facebook_tokens')->where('user_id', '=', $user->id)->first();
                if(!empty($fb_token)){
                    if($request->fb_token != $fb_token->token){
                        return $this->checkFBtoken($request->fb_token, $user, $request);
                        //return response()->json(['status'=>'error', 'error'=>['code'=>23, 'desc'=>'invalid token']]);
                    }
                    $token = DB::table('device_tokens')->where('user_id', '=', $user->id)->where('token', '=', $request->device_token)->first();
                    if (empty($token) && isset($request->device_token)){
                        DB::table('device_tokens')->insert(['user_id' => $user->id, 'token' => $request->device_token]);
                    }
                    $user_info = DB::table('user_notes')->where('user_id', '=', $user->id)->first();
                    $user_token = password_hash($user_info->age . $user_info->name . $user_info->city, PASSWORD_DEFAULT);
                    DB::table('users_tokens')->insert(['user_id'=>$user->id, 'token'=>$user_token]);
                    return response()->json(['status' => 'succes', 'user_id' => $user->id, 'user_token'=>$user_token]);
                }else{
                    return response()->json(['status'=>'error', 'error'=>['code'=>25, 'desc'=>'social token is empty']]);
                }

            }elseif(isset($request->vk_token)){

                $vk_token = DB::table('vkontakte_tokens')->where('user_id', '=', $user->id)->first();
                if(!empty($vk_token)){
                    if($request->vk_token != $vk_token->token){
                        return $this->checkVKtoken($request->vk_token, $user, $request);
                    }else {
                        $token = DB::table('device_tokens')->where('user_id', '=', $user->id)->where('token', '=', $request->device_token)->first();
                        if (empty($token) && isset($request->device_token)) {
                            DB::table('device_tokens')->insert(['user_id' => $user->id, 'token' => $request->device_token]);
                        }
                        $user_info = DB::table('user_notes')->where('user_id', '=', $user->id)->first();
                        $user_token = password_hash($user_info->age . $user_info->name . $user_info->city, PASSWORD_DEFAULT);
                        DB::table('users_tokens')->insert(['user_id' => $user->id, 'token' => $user_token]);
                        return response()->json(['status' => 'succes', 'user_id' => $user->id, 'user_token' => $user_token]);
                    }
                }else{
                    return response()->json(['status'=>'error', 'error'=>['code'=>25, 'desc'=>'social token is empty']]);
                }

            }
        }
    }

    public function checkUser(Request $request){
        if(!isset($request->email)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }else{
            $mail = DB::table('users')->where('email', '=', $request->email)->first();
            if(empty($mail)){
                return response()->json(['status'=>'free']);
            }else{
                return response()->json(['status'=>'busy']);
            }
        }
    }
            
     public function handleLogout(Request $request){
         if(!isset($request->user_id) || !isset($request->user_token)){
             return response()->json(['status'=>'error', 'error'=>[
                 'code'=>8,
                 'desc'=>'check parametrs list and their fill'
             ]]);
         }elseif( empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
             return response()->json(['status'=>'error', 'error'=>[
                 'code'=>2,
                 'desc'=>'user is not found'
             ]]);
         }elseif(!isset($request->device_token)){
             DB::table('users_tokens')->where('user_id', '=', $request->user_id)->where('token', '=', $request->user_token)->delete();
             DB::table('device_tokens')->where('user_id', '=', $request->user_id)->where('token', '=', $request->device_token)->delete();
             return response()->json(['status' => 'succes']);
         }else{
             DB::table('users_tokens')->where('user_id', '=', $request->user_id)->where('token', '=', $request->user_token)->delete();
             return response()->json(['status' => 'succes']);
         }
     }

    public function attachDeviceToken(Request $request){
        if(!isset($request->user_id) || !isset($request->device_token)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif( empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }elseif(!empty(DB::table('device_tokens')->where('token', '=', $request->device_token)->where('user_id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'success']);
        }else{
            DB::table('device_tokens')->insert(['user_id'=>$request->user_id, 'token'=>$request->device_token]);
            return response()->json(['status'=>'success']);
        }
    }

    public function checkVKtoken($token, $user, Request $request){

        $request_params = array(
            'access_token' => $token,
        );
        $get_params = http_build_query($request_params);
        $result = json_decode(file_get_contents('https://api.vk.com/method/users.get?'. $get_params));

        if(isset($result->response[0]->uid)){
            if(empty($vkId = DB::table('vkontakte_tokens')->where('user_id', '=', $user->id)->first())) return response()->json(['status'=>'error', 'error'=>['code'=>32, 'desc'=>'missing social information of user']]);
            $vkId = $vkId->vk_id;
            if($vkId != $result->response[0]->uid) return response()->json(['status'=>'error', 'error'=>['code'=>33, 'desc'=>'token behave for other user']]);

            DB::table('vkontakte_tokens')->where('user_id', '=', $user->id)->update(['token'=>$token]);

            $user_info = DB::table('user_notes')->where('user_id', '=', $user->id)->first();
            $user_token = password_hash($user_info->age . $user_info->name . $user_info->city, PASSWORD_DEFAULT);
            DB::table('users_tokens')->insert(['user_id'=>$user->id, 'token'=>$user_token]);

            $token = DB::table('device_tokens')->where('user_id', '=', $user->id)->where('token', '=', $request->device_token)->first();
            if (empty($token) && isset($request->device_token)){
                DB::table('device_tokens')->insert(['user_id' => $user->id, 'token' => $request->device_token]);
            }

            return response()->json(['status' => 'succes', 'user_id' => $user->id, 'user_token'=>$user_token]);
        }else{
            return response()->json(['status'=>'error', 'error'=>['code'=>23, 'desc'=>'invalid token']]);
        }

    }

    public function checkFBtoken($token, $user, Request $request){
        $request_params = array(
            'access_token' => $token,
            'fields'       => 'id,name'
        );
        $get_params = http_build_query($request_params);
        $result = json_decode(file_get_contents('https://graph.facebook.com/me?'. $get_params));

        if(isset($result->id)){
            if(empty($fbId = DB::table('facebook_tokens')->where('user_id', '=', $user->id)->first())) return response()->json(['status'=>'error', 'error'=>['code'=>32, 'desc'=>'missing social information of user']]);
            $fbId = $fbId->fb_id;
            if($fbId !== $result->id) return response()->json(['status'=>'error', 'error'=>['code'=>33, 'desc'=>'token behave for other user']]);

            DB::table('facebook_tokens')->where('user_id', '=', $user->id)->update(['token'=>$token]);

            $user_info = DB::table('user_notes')->where('user_id', '=', $user->id)->first();
            $user_token = password_hash($user_info->age . $user_info->name . $user_info->city, PASSWORD_DEFAULT);
            DB::table('users_tokens')->insert(['user_id'=>$user->id, 'token'=>$user_token]);

            $token = DB::table('device_tokens')->where('user_id', '=', $user->id)->where('token', '=', $request->device_token)->first();
            if (empty($token) && isset($request->device_token)){
                DB::table('device_tokens')->insert(['user_id' => $user->id, 'token' => $request->device_token]);
            }

            return response()->json(['status' => 'succes', 'user_id' => $user->id, 'user_token'=>$user_token]);
        }else{
            return response()->json(['status'=>'error', 'error'=>['code'=>23, 'desc'=>'invalid token']]);
        }
    }

    public function restorePassword(Request $request){
        if(!isset($request->email)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }else{
            $user = DB::table('users')->where('email', '=', $request->email)->first();
            if(empty($user)){
                return response()->json(['status'=>'error']);
            }else{
                $content = "Ваш новый пароль в приложении FitnessAndFoodApplication: $user->reserve_password";
                Mail::send('mails.mail', ['content' => $content], function($message) use($request)
                {
                    $message->from('FitnessAndfFood@gmail.com', 'FitnessAndFoodApplication')->subject('Restore password');
                    $message->to($request->email);
                });
                return response()->json(['status' => 'succes']);
            }
        }
    }

    public function changePass(Request $request, $user){
        $new_password = md5($user->reserve_password);
        $new_reserve_password = substr(md5(Hash::make($request->password)), -10, 10);
        DB::table('users')->where('email', '=', $request->email)->update(['password'=>$new_password, 'reserve_password'=>$new_reserve_password]);
    }
    
    public function adminLogin(Request $request, AdminUser $user){
        $validator = Validator::make(
            array('Логин' => $request->login,
                'Пароль'	=> $request->password
            ),
            array('Логин' => 'required|email',
                'Пароль' => 'required'
            ));
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $result = $user->login($request->login, $request->password);
        
        if($result === true){
            $request->session()->put('auth', $user->getHashByLogin($request->login));
            return redirect('/exercise-list');
        } else {
            $validator->after(function ($validator){
                    $validator->errors()->add('Пользователь', 'Пользователя с такой комбинацие логина и пароля не найдено!!');
            });
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }

    }

    public function adminLogout(Request $request){
        $request->session()->forget('auth');
        return redirect('/admin-login');
    }

    public function loginForm(Request $request, AdminUser $user){
        if ($request->session()->has('auth') && $user->checkHash($request->session()->get('auth'))) {
            return redirect('/exercise-list');
        }
        return view('dashboard.login');

    }
    

    
    
}
