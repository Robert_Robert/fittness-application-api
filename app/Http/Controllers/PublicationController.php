<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Models\Comment;
use App\Models\PubDisLike;
use App\Models\PubLike;
use App\Models\Publication;
use App\Models\PublicationAttachUser;
use App\Models\PublicationTag;
use Carbon\Carbon;
use App\User;
use App\Models\UserNote;
use App\Models\Subscribe;
use Illuminate\Http\Response;
use App\Models\PublicationComplaints;

class PublicationController extends Controller
{
    private $tagPattern = "/#(\d*[a-z.,]+\d*)/i";

    public function getSubscribers(Request $request, User $user){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif( empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }else{
            $result = [];
            $counter = 0;
            $user->id = $request->user_id;
            $subscribers = $user->subscribers()->select('subscriber_id')->get();

            foreach ($subscribers as $item){
                $note = DB::table('user_notes')->where('user_id', '=', $item->subscriber_id)->first();
                $result[$counter]['name'] = $note->name;
                $result[$counter]['avatar'] = $note->avatar;
                $result[$counter]['user_id'] = (int)$note->user_id;
                $counter++;
            }
            return response()->json(['status'=>'success', 'records'=>[
                'users'=>$result
            ]]);

        }
    }
    
    public function getSubscribes(Request $request, User $user){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif( empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }else{
            $result = [];
            $counter = 0;
            $user->id = $request->user_id;
            $subscribers = $user->subscribes()->select('cur_user_id')->get();

            foreach ($subscribers as $item){
                $note = DB::table('user_notes')->where('user_id', '=', $item->cur_user_id)->first();
                $result[$counter]['name'] = $note->name;
                $result[$counter]['avatar'] = $note->avatar;
                $result[$counter]['user_id'] = (int)$note->user_id;
                $counter++;
            }
            return response()->json(['status'=>'success', 'records'=>[
                'users'=>$result
            ]]);

        }
    }

    public function addPublication(Request $request, Publication $publication){
        if(!isset($request->text) || !isset($request->user_id) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);

        }elseif( empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'desc":"user is not found'
            ]]);
        }else{
            preg_match_all($this->tagPattern, $request->text, $result);
            $tags = !empty($result[0]) ? $result[0] : false;
            $text = preg_replace($this->tagPattern, '', $request->text);

            $dateTime = Carbon::now();
            $dateTime->timezone = "	Europe/London";
            $publication_id = Publication::insertGetId(
                ['content' => $text,
                 'user_id' => (int)$request->user_id,
                 'created_at'=>$dateTime,
                 'updated_at'=>$dateTime]
            );


            if($tags){
                foreach ($tags as $tag){
                    PublicationTag::insert(['publication_id'=>$publication_id, 'tag'=>$tag]);
                }
            }

            if(isset($request->attached_users) && !empty($request->attached_users)){
                if(gettype($request->attached_users) == 'array'){
                    foreach ($request->attached_users as $user){
                        PublicationAttachUser::insert(['publication_id'=>$publication_id, 'user_id'=>$user]);
                    }
                }else{
                    PublicationAttachUser::insert(['publication_id'=>$publication_id, 'user_id'=>$request->attached_users]);
                }
            }
            $publication->attachFile($request, $publication_id);
            return response()->json(['status'=>'success', 'records'=>[
                'publication_id'=>(int)$publication_id
            ]]);
        }
    }

    public function searchUsers(Request $request, UserNote $note){
        if(!isset($request->search_line) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }else{
            if(!empty($users  = $note->search($request))){
                return response()->json(['status'=>'success', 'records'=>[
                    'users'=>$users
                ]]);
            }else{
                return response()->json(['status'=>'error', 'error'=>[
                    'code'=>2,
                    'desc'=>'user is not found'
                ]]);
            }
        }
    }

    public function editPublication(Request $request, Publication $publication){
        if(!isset($request->text) || !isset($request->user_id) || !isset($request->publication_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif( empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }elseif(empty(Publication::where('id', '=', $request->publication_id)->where('user_id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>11,
                'desc'=>'users is not suitable for publication'
            ]]);
        }else{
            Publication::where('id', '=', $request->publication_id)->get()->each(function($item, $key){
                PublicationTag::where('publication_id', '=', $item->id)->delete();
                PublicationAttachUser::where('publication_id', '=', $item->id)->delete();

            });
            $dateTime = Carbon::now();
            $dateTime->timezone = "	Europe/London";
            Publication::where('id', '=', $request->publication_id)->update(
                ['content' => $request->text,
                    'content'=>$request->text,
                    'updated_at'=>$dateTime]
            );
            preg_match_all($this->tagPattern, $request->text, $result);
            $tags = !empty($result[0]) ? $result[0] : false;

            if($tags){
                foreach ($tags as $tag){
                    PublicationTag::insert(['publication_id'=>$request->publication_id, 'tag'=>$tag]);
                }
            }

            if(isset($request->attached_users) && !empty($request->attached_users)){
                if(gettype($request->attached_users) == 'array'){
                    foreach ($request->attached_users as $user){
                        PublicationAttachUser::insert(['publication_id'=>$request->publication_id, 'user_id'=>$user]);
                    }
                }else{
                    PublicationAttachUser::insert(['publication_id'=>$request->publication_id, 'user_id'=>$request->attached_users]);
                }
            }
            $publication->attachEditFile($request, $request->publication_id);

            $publication_obj = $this->getPublicationBuId($request);
            return $publication_obj;
//
//            return response()->json(['status'=>'success', 'records'=>[
//                'publication_id'=>(int)$request->publication_id
//            ]]);
        }
    }

    public function searchPublication(Request $request, Publication $publication){
        return $publication->searchPublication($request);
    }
    
    public function makeSubcribe(Request $request){
        if(!isset($request->user_id) || !isset($request->subscriber_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first()) || empty(DB::table('users')->where('id', '=', $request->subscriber_id)->first()) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(!empty(DB::table('subcribers')->where('cur_user_id', '=', $request->user_id)->where('subscriber_id', '=', $request->subscriber_id)->first()) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>18,
                'desc'=>'subscribe is already exist'
            ]]);
        }else{
            $subscribe_id = Subscribe::insertGetId(['cur_user_id'=>$request->user_id,
            'subscriber_id'=>$request->subscriber_id]);
            return response()->json(['status'=>'success', 'records'=>[
                'subscribe_id'=>(int)$subscribe_id
            ]]);
        }
    }

    public function makeUnSubcribe(Request $request){
        if(!isset($request->user_id) || !isset($request->subscriber_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first()) || empty(DB::table('users')->where('id', '=', $request->subscriber_id)->first()) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('subcribers')->where('cur_user_id', '=', $request->user_id)->where('subscriber_id', '=', $request->subscriber_id)->first()) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>34,
                'desc'=>'subscribe is missign'
            ]]);
        }else{
            DB::table('subcribers')->where('cur_user_id', '=', $request->user_id)->where('subscriber_id', '=', $request->subscriber_id)->delete();
            return response()->json(['status'=>'success']);
        }
    }



    public function getUserPublication(Request $request){
        if(!isset($request->user_id) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }else{
            $result = [];
            $counter = 0;
            $publications = DB::table('publication')
                ->where('publication.user_id', '=', $request->user_id)
                ->join('user_notes', 'publication.user_id', '=', 'user_notes.user_id')
                ->select('publication.*', 'user_notes.avatar', 'user_notes.name')
                ->orderBy('publication.created_at', 'desc')
                ->get();


            foreach ($publications as $publication){
                $files = DB::table('publication_files')->select('file', 'file_type')->where('publication_id', '=', $publication->id)->get()->toArray();
                $comments_count = DB::table('comments')->where('publication_id', '=', $publication->id)->count();
                $is_liked = !empty(DB::table('pub_like')->where('user_id', '=', $request->auth_user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
//                $is_disliked = !empty(DB::table('pub_dislike')->where('user_id', '=', $request->user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
                $tags = DB::table('publication_tags')->select('tag')->where('publication_id', '=', $publication->id)->get()->toArray();


                $like_amount = DB::table('pub_like')->where('publication_id', '=', $publication->id)->count();
//                $disLike_amount = DB::table('pub_dislike')->where('publication_id', '=', $publication->id)->count();
                $result[$counter]['publication_id'] = $publication->id;
                $result[$counter]['content'] = $publication->content;
                $result[$counter]['user']['user_id'] = (int)$publication->user_id;
                $result[$counter]['user']['avatar'] = $publication->avatar;
                $result[$counter]['user']['name'] = $publication->name;
                $result[$counter]['created_at'] = $publication->created_at;
                $result[$counter]['tags'] = $tags;
                $result[$counter]['comments_count'] = $comments_count;
                $result[$counter]['is_liked'] = $is_liked;
//                $result[$counter]['is_disliked'] = $is_disliked;
                $result[$counter]['like_amount'] = $like_amount;
//                $result[$counter]['disLike_amount'] = $disLike_amount;
                $result[$counter]['files'] = $files;
                $counter++;
            }
            return response()->json(['status'=>'success', 'records'=>[
                'publication'=>$result
            ]]);
        }
        
    }
    
    public function deletePublication(Request $request){

        if(!isset($request->user_id) || !isset($request->publication_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('publication')->where('id', '=', $request->publication_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>13,
                'desc'=>'this publication does not exist'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }else{
            $usPubId = DB::table('publication')->where('id', '=', $request->publication_id)->first()->user_id;
            if($usPubId != $request->user_id)   return response()->json(['status'=>'error', 'error'=>[
                'code'=>14,
                'desc'=>'the user is not author of publications'
            ]]);;
            DB::table('publication')->where('id', '=', $request->publication_id)->get()->each(function($item, $key){
                Comment::where('publication_id', '=', $item->id)->delete();
                PublicationAttachUser::where('publication_id', '=', $item->id)->delete();
                PublicationTag::where('publication_id', '=', $item->id)->delete();
                PubLike::where('publication_id', '=', $item->id)->delete();
//                PubDisLike::where('publication_id', '=', $item->id)->delete();
                Publication::where('id', '=', $item->id)->delete();
            });
            return response()->json(['status'=>'success']);
        }
    }
    
    public function likePub(Request $request){
        if(!isset($request->user_id) || !isset($request->publication_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('publication')->where('id', '=', $request->publication_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>13,
                'desc'=>'this publication does not exist'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }elseif(!empty(DB::table('pub_like')->where('user_id', '=', $request->user_id)->where('publication_id', '=', $request->publication_id)->first())) {
            DB::table('pub_like')->where('user_id', '=', $request->user_id)->where('publication_id', '=', $request->publication_id)->delete();
            $total = DB::table('pub_like')->where('publication_id', '=', $request->publication_id)->count();
            return response()->json(['status'=>'success', 'records'=>[
                'description'=>'like deleted',
                'publication_id'=>(int)$request->publication_id,
                'total'=>$total
            ]]);
        }else{
            PubLike::insert(['user_id'=>$request->user_id,
            'publication_id'=>(int)$request->publication_id]);

            $total = DB::table('pub_like')->where('publication_id', '=', $request->publication_id)->count();

            return response()->json(['status'=>'success', 'records'=>[
                'total'=>(int)$total
            ]]);
        }
    }

    public function disLikePub(Request $request, Publication $publication){
        return $publication->disLike($request);
    }

    public function getSubscribePub(Request $request){
        if(isset($request->search_type) ){
            if(!isset($request->page_size) || !isset($request->search_line) || !isset($request->user_id)) return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);

            if($request->search_type == 'tag'){
                $result = [];
                $counter = 0;
                if(isset($request->publication_id)) {
                    $start = $request->page_number * $request->page_size - $request->page_size;
                    $end = $request->page_size;


                    $users = Subscribe::select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->get();

                    $publications = collect();
                    foreach ($users as $user_item) {
                        $collection = DB::table('publication_tags')
                            ->where('tag', 'LIKE', '%' . $request->search_line . '%')
                            ->join('publication', function ($join) use ($user_item) {
                                $join->on('publication_tags.publication_id', '=', 'publication.id')
                                    ->where('publication.user_id', '=', $user_item->cur_user_id);
                            })
                            ->select('publication_tags.*', 'publication.*')
                            ->get();
                        $publications = $publications->merge($collection);
                    }
                    $publications = $publications->sortByDesc('created_at');
                    $total = $publications->filter(function($item, $key)use($request){
                        return $item->id < $request->publication_id;
                    })->count();
                    $publications = $publications->filter(function($item, $key)use($request){
                        return $item->id < $request->publication_id;
                    })->slice(0, (int)$request->page_size);
                }elseif(isset($request->page_number)){
                    $start = $request->page_number * $request->page_size - $request->page_size;
                    $end = $request->page_size;


                    $users = Subscribe::select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->get();

                    $publications = collect();
                    foreach ($users as $user_item) {
                        $collection = DB::table('publication_tags')
                            ->where('tag', 'LIKE', '%' . $request->search_line . '%')
                            ->join('publication', function ($join) use ($user_item) {
                                $join->on('publication_tags.publication_id', '=', 'publication.id')
                                    ->where('publication.user_id', '=', $user_item->cur_user_id);
                            })
                            ->select('publication_tags.*', 'publication.*')
                            ->get();
                        $publications = $publications->merge($collection);
                    }
                    $publications = $publications->sortByDesc('created_at');
                    $total = $publications->count();
                    $publications = $publications->forPage($request->page_number, $request->page_size);
                }else{
                    return response()->json(['status'=>'error', 'error'=>['code'=>8, 'desc'=>'check parametrs list and their fill']]);
                }

                foreach ($publications as $publication){
                    $user_info = DB::table('user_notes')->select('name', 'avatar')->where('user_id', '=', $publication->user_id)->first();
                    $files = DB::table('publication_files')->select('file', 'file_type')->where('publication_id', '=', $publication->id)->get()->toArray();
                    $comments_count = DB::table('comments')->where('publication_id', '=', $publication->id)->count();
                    $is_liked = !empty(DB::table('pub_like')->where('user_id', '=', $request->auth_user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
                    $tags = DB::table('publication_tags')->select('tag')->where('publication_id', '=', $publication->id)->get()->toArray();

                    $like_amount = DB::table('pub_like')->where('publication_id', '=', $publication->publication_id)->count();
                    $result[$counter]['publication_id'] = (int)$publication->id;
                    $result[$counter]['content'] = $publication->content;
                    $result[$counter]['user']['user_id'] = (int)$publication->user_id;
                    $result[$counter]['user']['avatar'] = $user_info->avatar;
                    $result[$counter]['user']['name'] = $user_info->name;
                    $result[$counter]['files'] = $files;
                    $result[$counter]['created_at'] = $publication->created_at;
                    $result[$counter]['tags'] = $tags;
                    $result[$counter]['comments_count'] = $comments_count;
                    $result[$counter]['is_liked'] = $is_liked;
                    $result[$counter]['like_amount'] = $like_amount;
                    $counter++;
                }
                return response()->json(['status'=>'success', 'records'=>[
                    'publication'=>$result,
                    'total'=>$total
                ]]);
            }elseif ($request->search_type == 'name'){
                $result = [];
                $counter = 0;
                if(isset($request->page_number)) {
                    $users = Subscribe::select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->get();
                    $publications = collect();
                    foreach ($users as $user_item) {
                        $collection = DB::table('user_notes')
                            ->where('name', 'LIKE', '%' . $request->search_line . '%')
                            ->join('publication', function ($join) use ($user_item) {
                                $join->on('user_notes.user_id', '=', 'publication.user_id')
                                    ->where('publication.user_id', '=', $user_item->cur_user_id);
                            })
                            ->select('user_notes.name', 'user_notes.avatar', 'user_notes.user_id', 'publication.content', 'publication.id', 'publication.created_at')
                            ->get();
                        $publications = $publications->merge($collection);
                    }
                    $publications = $publications->sortByDesc('created_at');
                    $total = $publications->count();
                    $publications = $publications->forPage($request->page_number, $request->page_size);

                }elseif(isset($request->publication_id)){

                    $users = Subscribe::select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->get();
                    $publications = collect();
                    foreach ($users as $user_item) {
                        $collection = DB::table('user_notes')
                            ->where('name', 'LIKE', '%' . $request->search_line . '%')
                            ->join('publication', function ($join) use ($user_item) {
                                $join->on('user_notes.user_id', '=', 'publication.user_id')
                                    ->where('publication.user_id', '=', $user_item->cur_user_id);
                            })
                            ->select('user_notes.name', 'user_notes.avatar', 'user_notes.user_id', 'publication.content', 'publication.id', 'publication.created_at')
                            ->get();
                        $publications = $publications->merge($collection);
                    }
                    $publications = $publications->sortByDesc('created_at');
                    $total = $publications->filter(function($item, $key)use($request){
                        return $item->id < $request->publication_id;
                    })->count();
                    $publications = $publications->filter(function($item, $key)use($request){
                        return $item->id < $request->publication_id;
                    })->slice(0, (int)$request->page_size);
                }else{
                    return response()->json(['status'=>'error', 'error'=>['code'=>8, 'desc'=>'check parametrs list and their fill']]);
                }
                foreach ($publications as $publication){
                    $files = DB::table('publication_files')->select('file', 'file_type')->where('publication_id', '=', $publication->id)->get()->toArray();
                    $comments_count = DB::table('comments')->where('publication_id', '=', $publication->id)->count();
                    $is_liked = !empty(DB::table('pub_like')->where('user_id', '=', $request->auth_user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
//                    $is_disliked = !empty(DB::table('pub_dislike')->where('user_id', '=', $request->user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
                    $tags = DB::table('publication_tags')->select('tag')->where('publication_id', '=', $publication->id)->get()->toArray();

                    $like_amount = DB::table('pub_like')->where('publication_id', '=', $publication->id)->count();
//                    $disLike_amount = DB::table('pub_dislike')->where('publication_id', '=', $publication->id)->count();
                    $result[$counter]['publication_id'] = (int)$publication->id;
                    $result[$counter]['content'] = $publication->content;
                    $result[$counter]['user']['user_id'] = (int)$publication->user_id;
                    $result[$counter]['user']['avatar'] = $publication->avatar;
                    $result[$counter]['user']['name'] = $publication->name;
                    $result[$counter]['files'] = $files;
                    $result[$counter]['created_at'] = $publication->created_at;
                    $result[$counter]['tags'] = $tags;
                    $result[$counter]['comments_count'] = $comments_count;
                    $result[$counter]['is_liked'] = $is_liked;
//                    $result[$counter]['is_disliked'] = $is_disliked;
                    $result[$counter]['like_amount'] = $like_amount;
//                    $result[$counter]['disLike_amount'] = $disLike_amount;
                    $counter++;
                }
                return response()->json(['status'=>'succes', 'records'=>[
                    'publication'=>$result,
                    'total'=>$total
                ]]);
            }else{
                return response()->json(['status'=>'error', 'error'=>[
                    'code'=>15,
                    'desc'=>'unsupported serch type'
                ]]);
            }

        } elseif(!isset($request->user_id) ||  !isset($request->page_size)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);

        }else{
            $result = [];
            $counter = 0;
            if(isset($request->page_number)) {
                $users = Subscribe::select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->get();
                $collections = collect();
                foreach ($users as $user) {
                    $publications = DB::table('publication')
                        ->where('publication.user_id', '=', $user->cur_user_id)
                        ->join('user_notes', 'publication.user_id', '=', 'user_notes.user_id')
                        ->select('publication.id', 'publication.user_id', 'publication.content', 'publication.created_at', 'user_notes.avatar', 'user_notes.name')
                        ->orderBy('publication.created_at', 'desc')
                        ->get();
                    $collections = $collections->merge($publications);
                }
                $collections = $collections->sortByDesc('created_at');
                $total = $collections->count();
                $collections = $collections->forPage($request->page_number, $request->page_size);
                // получил последние с учетом page_number и page_size публикации пользователей и отсортировал по дате

            }elseif(isset($request->publication_id)){
                $users = Subscribe::select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->get();
                $collections = collect();
                foreach ($users as $user) {
                    $publications = DB::table('publication')
                        ->where('publication.user_id', '=', $user->cur_user_id)
                        ->join('user_notes', 'publication.user_id', '=', 'user_notes.user_id')
                        ->select('publication.id', 'publication.user_id', 'publication.content', 'publication.created_at', 'user_notes.avatar', 'user_notes.name')
                        ->orderBy('publication.created_at', 'desc')
                        ->get();
                    $collections = $collections->merge($publications);
                }
                $collections = $collections->sortByDesc('created_at');
                $total = $collections->filter(function($item, $key)use($request){
                    return $item->id < $request->publication_id;
                })->count();
                $collections = $collections->filter(function($item, $key)use($request){
                    return $item->id < $request->publication_id;
                })->slice(0, (int)$request->page_size);
            }else{
                return response()->json(['status'=>'error', 'error'=>['code'=>8, 'desc'=>'check parametrs list and their fill']]);
            }
            foreach ($collections as $collection){
                $files = DB::table('publication_files')->select('file', 'file_type')->where('publication_id', '=', $collection->id)->get()->toArray();
                $comments_count = DB::table('comments')->where('publication_id', '=', $collection->id)->count();
                $is_liked = !empty(DB::table('pub_like')->where('user_id', '=', $request->auth_user_id)->where('publication_id', '=', $collection->id)->first()) ? true : false;
                $tags = DB::table('publication_tags')->select('tag')->where('publication_id', '=', $collection->id)->get()->toArray();

                $like_amount = DB::table('pub_like')->where('publication_id', '=', $collection->id)->count();
                $result[$counter]['publication_id'] = (int)$collection->id;
                $result[$counter]['content'] = $collection->content;
                $result[$counter]['user']['user_id'] = (int)$collection->user_id;
                $result[$counter]['user']['avatar'] = $collection->avatar;
                $result[$counter]['user']['name'] = $collection->name;
                $result[$counter]['files'] = $files;
                $result[$counter]['created_at'] = $collection->created_at;
                $result[$counter]['tags'] = $tags;
                $result[$counter]['comments_count'] = $comments_count;
                $result[$counter]['is_liked'] = $is_liked;
                $result[$counter]['like_amount'] = $like_amount;
                $counter++;
            }

            return response()->json(['status'=>'success', 'records'=>[
                'publication'=>$result,
                'total'      =>$total
            ]]);
        }
    }
    
    public function getComments(Request $request){
            if(!isset($request->page_number) || !isset($request->page_size) || !isset($request->publication_id)) {
                return response()->json(['status' => 'error', 'error' => [
                    'code' => 8,
                    'desc' => 'check parametrs list and their fill'
                ]]);
            }elseif(empty(DB::table('publication')->where('id', '=', $request->publication_id)->first())){
                return response()->json(['status'=>'error', 'error'=>[
                    'code'=>13,
                    'desc'=>'this publication does not exist'
                ]]);
            }else{
                $start = $request->page_number * $request->page_size - $request->page_size;
                $end   = $request->page_size;
                $comments = DB::table('comments')
                    ->where('publication_id', '=', $request->publication_id)
                    ->join('user_notes', 'comments.user_id', '=', 'user_notes.user_id')
                    ->select('comments.content', 'comments.created_at', 'comments.id', 'user_notes.name', 'user_notes.avatar', 'user_notes.user_id')
                    ->orderBy('comments.created_at', 'asc')
                    ->skip($start)
                    ->take($end)
                    ->get();
                $result = [];
                $counter = 0;
                foreach ($comments as $comment){
                    $result[$counter]['publication_id'] = (int)$request->publication_id;
                    $result[$counter]['content'] = $comment->content;
                    $result[$counter]['created_at'] = $comment->created_at;
                    $result[$counter]['comment_id'] = (int)$comment->id;
                    $result[$counter]['user']['name'] = $comment->name;
                    $result[$counter]['user']['avatar'] = $comment->avatar;
                    $result[$counter]['user']['user_id'] = (int)$comment->user_id;
                    $counter++;
                }

            }
        return response()->json(['status'=>'success', 'records'=>[
            'comments'=>$result,
            'total'=>$counter

        ]]);

    }

    public function getAttachUsers(Request $request){
        if(!isset($request->publication_id)) {
            return response()->json(['status' => 'error', 'error' => [
                'code' => 8,
                'desc' => 'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('publication')->where('id', '=', $request->publication_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>13,
                'desc'=>'this publication does not exist'
            ]]);
        }else{
            $attach_users = DB::table('publication_attach_users')
                    ->where('publication_id', '=', $request->publication_id)
                    ->join('user_notes', 'publication_attach_users.user_id', '=', 'user_notes.user_id')
                    ->select('publication_attach_users.user_id', 'user_notes.name')
                    ->get()->toArray();
            return response()->json(['status'=>'success', 'records'=>[
                'users'=>$attach_users,
                'publication_id'=>(int)$request->publication_id
            ]]);
        }
    }
    
    public function getPublicationBuId(Request $request){
        if(!isset($request->publication_id)) {
            return response()->json(['status' => 'error', 'error' => [
                'code' => 8,
                'desc' => 'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('publication')->where('id', '=', $request->publication_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>13,
                'desc'=>'this publication does not exist'
            ]]);
        }else{
            $publication = DB::table('publication')
                ->where('publication.id', '=', $request->publication_id)
                ->join('user_notes', 'publication.user_id', '=', 'user_notes.user_id')
                ->select('publication.id', 'publication.user_id', 'publication.content', 'publication.created_at', 'user_notes.avatar', 'user_notes.name')
                ->orderBy('publication.created_at', 'desc')
                ->first();
            $files = DB::table('publication_files')->select('file', 'file_type')->where('publication_id', '=', $publication->id)->get()->toArray();
            $comments_count = DB::table('comments')->where('publication_id', '=', $publication->id)->count();
            $is_liked = !empty(DB::table('pub_like')->where('user_id', '=', $request->auth_user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
//            $is_disliked = !empty(DB::table('pub_dislike')->where('user_id', '=', $request->user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
            $tags = DB::table('publication_tags')->select('tag')->where('publication_id', '=', $publication->id)->get()->toArray();
            $like_amount = DB::table('pub_like')->where('publication_id', '=', $request->publication_id)->count();
            $result = ['publication_id'=>(int)$publication->id,
                       'content'=>$publication->content,
                       'created_at'=>$publication->created_at,
                       'comments_count'=>$comments_count,
                       'like_amount'=>$like_amount,
                       'is_liked'=>$is_liked,
                       'tags'=>$tags,
                       'files'=>$files,
                       'user'=>[
                           'user_id'=>(int)$publication->id,
                           'name'=>$publication->name,
                           'avatar'=>$publication->avatar
                       ]];
            return response()->json(['status'=>'success', 'records'=>[
                'publication'=>$result
            ]]);

        }
    }

    public function complaintPublication(Request $request, PublicationComplaints $complaint, Publication $publication){
        if(!isset($request->user_id) || !isset($request->publication_id) || !isset($request->reason)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }elseif(empty(DB::table('publication')->where('id', '=', $request->publication_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>13,
                'desc'=>'this publication does not exist'
            ]]);
        }else{
            return $publication->complaintPublication($request, $complaint);
        }
    }

    public function leaveComment(Request $request, Publication $publication){
        return $publication->addComment($request);
    }
    
    public function editComment(Request $request, Publication $publication){
        return $publication->editComment($request);
    }

    public function deleteComment(Request $request, Publication $publication){
        return $publication->deleteComment($request);
    }
}

