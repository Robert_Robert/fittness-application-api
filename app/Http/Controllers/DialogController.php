<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Models\Dialog;
use App\Models\Message;
use App\Models\MessageContent;
use App\Models\BlockedDialog;
use App\Models\Subscribe;
use Carbon\Carbon;
use Illuminate\Http\Response;
use PushNotification;
use App\Models\DeviceToken;
use App\Models\UsersBadge;

class DialogController extends Controller
{

    public function createDialog(Request $request, Dialog $dialog, UsersBadge $badge){

        $userOne = $request->user_1;
        $userTwo = $request->user_2;


        $cheUs1 = DB::table('users')->where('id', '=', (int)$userOne)->first();
        $cheUs2 = DB::table('users')->where('id', '=', (int)$userTwo)->first();

        $user_two_info = DB::table('user_notes')->select('name', 'avatar', 'user_id')->where('user_id', '=', $userTwo)->first();

        if(!isset($request->user_1) || !isset($request->user_2)){  // проверяю переданы ли пользователи
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>1,
                'desc'=>'check the number of users'
            ]]);

        } elseif (empty($cheUs1) || empty($cheUs2)){  //если пользователи переданы, проверяю валидность пользователей
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);

        } elseif(!empty(DB::table('dialogs')->where('user_1', '=', $userOne)->where('user_2', '=', $userTwo)->first()) || !empty(DB::table('dialogs')->where('user_1', '=', $userTwo)->where('user_2', '=', $userOne)->first()) ){ //проверяю существует ли такой диалог


            if(!empty(DB::table('dialogs')->where('user_1', '=', $userOne)->where('user_2', '=', $userTwo)->first())){ //если существует, проверяю первую комбинацию пользователей (диалог с user_1 user_2)
                if(DB::table('dialogs')->where('user_1', '=', $userOne)->where('user_2', '=', $userTwo)->first()->unvisible_user == $request->user_1 ){
                    DB::table('dialogs')->where('user_1', '=', $userOne)->where('user_2', '=', $userTwo)->update(['unvisible_user'=>0]); //если дошло сюда, значит диалог user_1 с _user_2 существовал и его надо возобновить
                    $result = [];
                    $dialog = DB::table('dialogs')->where('user_1', '=', $userOne)->where('user_2', '=', $userTwo)->first();
                    $last_message = DB::table('messages')->where('dialog_id', '=', $dialog->id)->orderBy('created_at', 'desc')->first();
                    (!empty($last_message)) ? $result['last_message_date'] = $last_message->created_at : '';
                    (!empty($last_message)) ? $result['last_message'] = DB::table('message_contents')->where('id', '=', $last_message->content_id)->first()->content : '';
                    $result['dialog_id'] = $dialog->id;
                    $result['user']['avatar'] = $user_two_info->avatar;
                    $result['user']['name'] = $user_two_info->name;
                    $result['user']['user_id'] = $user_two_info->user_id;
                    $result['badge'] = $badge->getBadgeByUserAndDialogId($request->user_1, $dialog->id);
                    return response()->json(['status'=>'succes', 'records'=>$result]);

                }elseif(DB::table('dialogs')->where('user_1', '=', $userOne)->where('user_2', '=', $userTwo)->first()->unvisible_user == 0){
                    $result = [];
                    $dialog = DB::table('dialogs')->where('user_1', '=', $userOne)->where('user_2', '=', $userTwo)->first();
                    $last_message = DB::table('messages')->where('dialog_id', '=', $dialog->id)->orderBy('created_at', 'desc')->first();
                    (!empty($last_message)) ? $result['last_message_date'] = $last_message->created_at : '';
                    (!empty($last_message)) ? $result['last_message'] = DB::table('message_contents')->where('id', '=', $last_message->content_id)->first()->content : '';
                    $result['dialog_id'] = $dialog->id;
                    $result['user']['avatar'] = $user_two_info->avatar;
                    $result['user']['name'] = $user_two_info->name;
                    $result['user']['user_id'] = $user_two_info->user_id;
                    $result['badge'] = $badge->getBadgeByUserAndDialogId($request->user_1, $dialog->id);
                    return response()->json(['status'=>'succes', 'records'=>$result]);

                }elseif(DB::table('dialogs')->where('user_1', '=', $userOne)->where('user_2', '=', $userTwo)->first()->unvisible_user == $request->user_2){
                    $result = [];
                    $dialog = DB::table('dialogs')->where('user_1', '=', $userOne)->where('user_2', '=', $userTwo)->first();
                    $last_message = DB::table('messages')->where('dialog_id', '=', $dialog->id)->orderBy('created_at', 'desc')->first();
                    (!empty($last_message)) ? $result['last_message_date'] = $last_message->created_at : '';
                    (!empty($last_message)) ? $result['last_message'] = DB::table('message_contents')->where('id', '=', $last_message->content_id)->first()->content : '';
                    $result['dialog_id'] = $dialog->id;
                    $result['user']['avatar'] = $user_two_info->avatar;
                    $result['user']['name'] = $user_two_info->name;
                    $result['user']['user_id'] = $user_two_info->user_id;
                    $result['badge'] = $badge->getBadgeByUserAndDialogId($request->user_1, $dialog->id);
                    return response()->json(['status'=>'success', 'records'=>$result]);
                }
            }elseif(!empty(DB::table('dialogs')->where('user_1', '=', $userTwo)->where('user_2', '=', $userOne)->first())){ //если существует, проверяю вторую комбинацию пользователей (диалог с user_2 user_1)
                if(DB::table('dialogs')->where('user_1', '=', $userTwo)->where('user_2', '=', $userOne)->first()->unvisible_user == $request->user_2 ){
                    $result = [];
                    $dialog = DB::table('dialogs')->where('user_1', '=', $userTwo)->where('user_2', '=', $userOne)->first();
                    $last_message = DB::table('messages')->where('dialog_id', '=', $dialog->id)->orderBy('created_at', 'desc')->first();
                    (!empty($last_message)) ? $result['last_message_date'] = $last_message->created_at : '';
                    (!empty($last_message)) ? $result['last_message'] = DB::table('message_contents')->where('id', '=', $last_message->content_id)->first()->content : '';
                    $result['dialog_id'] = $dialog->id;
                    $result['user']['avatar'] = $user_two_info->avatar;
                    $result['user']['name'] = $user_two_info->name;
                    $result['user']['user_id'] = $user_two_info->user_id;
                    $result['badge'] = $badge->getBadgeByUserAndDialogId($request->user_1, $dialog->id);
                    return response()->json(['status'=>'success', 'records'=>$result]);
                }elseif(DB::table('dialogs')->where('user_1', '=', $userTwo)->where('user_2', '=', $userOne)->first()->unvisible_user == 0){
                    $result = [];
                    $dialog = DB::table('dialogs')->where('user_1', '=', $userTwo)->where('user_2', '=', $userOne)->first();
                    $last_message = DB::table('messages')->where('dialog_id', '=', $dialog->id)->orderBy('created_at', 'desc')->first();
                    (!empty($last_message)) ? $result['last_message_date'] = $last_message->created_at : '';
                    (!empty($last_message)) ? $result['last_message'] = DB::table('message_contents')->where('id', '=', $last_message->content_id)->first()->content : '';
                    $result['dialog_id'] = $dialog->id;
                    $result['user']['avatar'] = $user_two_info->avatar;
                    $result['user']['name'] = $user_two_info->name;
                    $result['user']['user_id'] = $user_two_info->user_id;
                    $result['badge'] = $badge->getBadgeByUserAndDialogId($request->user_1, $dialog->id);
                    return response()->json(['status'=>'success', 'records'=>$result]);
                }elseif (DB::table('dialogs')->where('user_1', '=', $userTwo)->where('user_2', '=', $userOne)->first()->unvisible_user == $request->user_1){
                    DB::table('dialogs')->where('user_1', '=', $userTwo)->where('user_2', '=', $userOne)->update(['unvisible_user'=>0]); //если дошло сюда, значит диалог user_1 с _user_2 существовал и его надо возобновить
                    $result = [];
                    $dialog = DB::table('dialogs')->where('user_1', '=', $userTwo)->where('user_2', '=', $userOne)->first();
                    $last_message = DB::table('messages')->where('dialog_id', '=', $dialog->id)->orderBy('created_at', 'desc')->first();
                    (!empty($last_message)) ? $result['last_message_date'] = $last_message->created_at : '';
                    (!empty($last_message)) ? $result['last_message'] = DB::table('message_contents')->where('id', '=', $last_message->content_id)->first()->content : '';
                    $result['dialog_id'] = $dialog->id;
                    $result['user']['avatar'] = $user_two_info->avatar;
                    $result['user']['name'] = $user_two_info->name;
                    $result['user']['user_id'] = $user_two_info->user_id;
                    $result['badge'] = $badge->getBadgeByUserAndDialogId($request->user_1, $dialog->id);
                    return response()->json(['status'=>'success', 'records'=>$result]);
                }
            }

        }

        $result = [];
        $result['user']['avatar'] = $user_two_info->avatar;
        $result['user']['name'] = $user_two_info->name;
        $result['user']['user_id'] = $user_two_info->user_id;

        $dialog->user_1 = $userOne;
        $dialog->user_2 = $userTwo;
        $dialog->unvisible_user = 0;
        $dialog->save();

        DB::table('users_badges')->insert(['dialog_id'=>$dialog->id, 'user_id'=>$userOne,'unreaded_count'=>0]);
        DB::table('users_badges')->insert(['dialog_id'=>$dialog->id, 'user_id'=>$userTwo,'unreaded_count'=>0]);

        return response()->json(['status'=>'succes', 'records'=>[
            'dialog_id'=>(int)$dialog->id,
            'user'=>$result['user']
        ]]);

    }

    public function deleteteDialog(Request $request, Dialog $dialog){
        if(!isset($request->dialog_id)) {
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>4,
                'desc'=>'empty dialog id value'
            ]]);
        }elseif(empty(DB::table('dialogs')->where('id', '=', $request->dialog_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>5,
                'desc'=>'missing dialog id in DataBase'
            ]]);
        } elseif(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>6,
                'desc'=>'empty user id value'
            ]]);
        } elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>5,
                'desc'=>'missing dialog id in DataBase'
            ]]);
        }elseif(!empty(DB::table('dialogs')->where('id', '=', $request->dialog_id)->first())){
            $userOne = DB::table('dialogs')->where('id', '=', $request->dialog_id)->first()->user_1;
            $userTwo = DB::table('dialogs')->where('id', '=', $request->dialog_id)->first()->user_2;
            if($request->user_id != $userOne && $request->user_id != $userTwo) return response()->json(['status'=>'error', 'error'=>[
                'code'=>9,
                'desc'=>'user is from another dialog'
            ]]);;
            if(DB::table('dialogs')->where('id', '=', $request->dialog_id)->first()->unvisible_user	== $request->user_id){
                return response()->json(['status'=>'error', 'error'=>[
                    'code'=>7,
                    'desc'=>'dialog is droped'
                ]]);
            }elseif(DB::table('dialogs')->where('id', '=', $request->dialog_id)->first()->unvisible_user == 0){
                DB::table('dialogs')->where('id', '=', $request->dialog_id)->update(['unvisible_user'=>$request->user_id]);
                $dialog->id = $request->dialog_id;
                $dialog->messages()->each(function($item, $key) use ($request){
                   if($item->hidden_for_user == 0) $item->hidden_for_user = $request->user_id;
                    $item->save();
                });
                return response()->json(['status'=>'succes', 'records'=>[
                    'dialog_id'=>(int)$request->dialog_id
                ]]);
            }elseif(DB::table('dialogs')->where('id', '=', $request->dialog_id)->first()->unvisible_user != 0 && DB::table('dialogs')->where('id', '=', $request->dialog_id)->first()->unvisible_user != $request->user_id){
                $dialog->id = $request->dialog_id;
                $dialog->messages()->get()->each(function($item, $key){
                    $item->content()->first()->delete();
                    $item->delete();
                });
                $dialog = DB::table('dialogs')->where('id', '=', $request->dialog_id)->first();
                UsersBadge::deleteBadge($dialog->id);
                DB::table('dialogs')->where('id', '=', $request->dialog_id)->delete();
                return response()->json(['status'=>'succes', 'records'=>[
                    'dialog_id'=>(int)$request->dialog_id
                ]]);

            }
        }


    }

    public function addMessage(Request $request, Dialog $dialog, Message $message, MessageContent $messageContent, DeviceToken $deviceToken, UsersBadge $badge){
        if(!isset($request->dialog_id) || !isset($request->user_id_from) || !isset($request->user_id_to) || !isset($request->message)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
           // return response()->setStatusCode(400, '{"status":"error", "error":{"code": 8, "desc":"check parametrs list and their fill"}}');
        }elseif(!empty(DB::table('dialogs')->where('id', '=', $request->dialog_id)->first())){
            if(!empty(DB::table('dialogs')->where('user_1', '=', $request->user_id_from)->where('user_2', '=', $request->user_id_to)->first()) || !empty(DB::table('dialogs')->where('user_1', '=', $request->user_id_to)->where('user_2', '=', $request->user_id_from)->first()) ) { //проверяю существует ли такой диалог
                if(DB::table('dialogs')->where('id', '=', $request->dialog_id)->first()->unvisible_user != 0){
                    if(!empty(DB::table('blocked_dialogs')->where('id', '=', $request->dialog_id)->where('blocked_user', '=', $request->user_id_from )->first())){
                        return response()->json(['status'=>'error', 'error'=>[
                            'code'=>11,
                            'desc'=>'User blocked you. You can not send messages.'
                        ]]);
                    }
                    DB::table('dialogs')->where('id', '=', $request->dialog_id)->update(['unvisible_user' => 0]);
                    $contentId = DB::table('message_contents')->insertGetId(
                        ['content' => $request->message]
                    );
                    $dateTime = Carbon::now();
                    $dateTime->timezone = "Europe/London";

                    $mes_id =  DB::table('messages')->insertGetId(
                        ['dialog_id' =>(int)$request->dialog_id,
                         'to_user_id' => $request->user_id_to,
                         'from_user_id' => $request->user_id_from,
                         'content_id' =>(int)$contentId,
                         'hidden_for_user' => 0,
                         'created_at' => $dateTime
                         ]);


                    $message->attachFile($request, $mes_id);

                    $badge->addOneBadge($request->dialog_id, $request->user_id_to);


                    $tokens = $deviceToken->getTokensById($request->user_id_to);
                    $user_name = DB::table('user_notes')->where('user_id', '=', $request->user_id_to)->first();
                    $badges = $badge->getTotalBagde($request->user_id_to); //получаю число всех баджей пользователя(непрочитаных сообщений у всех диалогах)


                    $devices = PushNotification::DeviceCollection($tokens);


                    $messages = PushNotification::Message($request->message, array(
                        'badge' => $badges,
                        'sound' => 'example.aiff',

                        'actionLocKey' => 'Action button title!',
                        'locKey' => 'localized key',
                        'title'  => $user_name->name,
                        'locArgs' => array(
                            'localized args',
                            'localized args',
                        ),
                        'launchImage' => 'image.jpg',

                        'custom' => array('additional' => array(
                            'dialog_id' => $request->dialog_id,
                            'action'    => 'open_dialog',
                        ))
                    ));

                    $collection = PushNotification::app('fitnesAppIOS')
                        ->to($devices)
                        ->send($messages);

                    foreach ($collection->pushManager as $push) {
                        $response = $push->getAdapter()->getResponse();
                    }


                    return response()->json(['status'=>'succes', 'records'=>[
                        'message_id'=>(int)$mes_id,
                        'time'=>$dateTime
                    ]]);
                }else{
                    if(!empty(DB::table('blocked_dialogs')->where('dialog_id', '=', $request->dialog_id)->where('blocked_user', '=', $request->user_id_from )->first())){
                        return response()->json(['status'=>'error', 'error'=>[
                            'code'=>11,
                            'desc'=>'User blocked you. You can not send messages.'
                        ]]);
                    }
                    $contentId = DB::table('message_contents')->insertGetId(
                        ['content' => $request->message]
                    );
                    $dateTime = Carbon::now();
                    $dateTime->timezone = "	Europe/London";


                    $mes_id =  DB::table('messages')->insertGetId(
                        ['dialog_id' =>(int)$request->dialog_id,
                            'to_user_id' => $request->user_id_to,
                            'from_user_id' =>$request->user_id_from,
                            'content_id' =>(int)$contentId,
                            'hidden_for_user' => 0,
                            'created_at' => $dateTime
                        ]);

                    $message->attachFile($request, $mes_id);

                    $badge->addOneBadge($request->dialog_id, $request->user_id_to);

                    $tokens = $deviceToken->getTokensById($request->user_id_to);
                    $user_name = DB::table('user_notes')->where('user_id', '=', $request->user_id_to)->first();
                    $badges = $badge->getTotalBagde($request->user_id_to); //получаю число всех баджей пользователя(непрочитаных сообщений у всех диалогах)

                    $devices = PushNotification::DeviceCollection($tokens);


                    $messages = PushNotification::Message($request->message, array(
                        'badge' => $badges,
                        'sound' => 'example.aiff',

                        'actionLocKey' => 'Action button title!',
                        'locKey' => 'localized key',
                        'title'  => $user_name->name,
                        'locArgs' => array(
                            'localized args',
                            'localized args',
                        ),
                        'launchImage' => 'image.jpg',

                        'custom' => array('additional' => array(
                            'dialog_id' => $request->dialog_id,
                            'action'    => 'open_dialog',
                        ))
                    ));

                    $collection = PushNotification::app('fitnesAppIOS')
                        ->to($devices)
                        ->send($messages);

                    foreach ($collection->pushManager as $push) {
                        $response = $push->getAdapter()->getResponse();
                    }


                    return response()->json(['status'=>'succes', 'records'=>[
                        'message_id'=>(int)$mes_id,
                        'time'=>$dateTime
                    ]]);
                }
            }else{
                return response()->json(['status'=>'error', 'error'=>[
                    'code'=>9,
                    'desc'=>'the list of users is not suitable for dialog'
                ]]);
            }
        }elseif(empty(DB::table('dialogs')->where('id', '=', $request->dialog_id)->first())){
            //return response()->setStatusCode(400, '{"status":"error", "error":{"code": 5, "desc":"missing dialog id in DataBase"}}');
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>5,
                'desc'=>'missing dialog id in DataBase'
            ]]);
        }

    }

    public function addFile(Request $request){
        if(!isset($request->file) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>10,
                'desc'=>'emty file'
            ]]);
        }else{
            if(gettype($request->file) == 'array'){
                $count = 0;
                $array = [];
                foreach ($request->file as $item) {
                    $filename = uniqid('file_').'.'.$item->getClientOriginalExtension();
                    $path = $item->move('public/users_files', $filename);
                    $array[$count] = $_SERVER['SERVER_NAME'].'/'.(string)$path;
                    $count++;
                }
                return response()->json(['status'=>'succes', 'records'=>[
                    'filesUrl'=>$array
                ]]);
            }else{
                $filename = uniqid('file_').'.'.$request->file->getClientOriginalExtension();
                $path = $request->file->move('public/users_files', $filename);
                return response()->json(['status'=>'succes', 'records'=>[
                    'fileUrl'=>$_SERVER['SERVER_NAME'].'/'.(string)$path
                ]]);

            }
        }
    }

    public function getHistory(Request $request){
        if(!isset($request->dialog_id) || !isset($request->page_number) || !isset($request->page_size) || !isset($request->user_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }else{
            $start = $request->page_number * $request->page_size - $request->page_size;
            $end   = $request->page_size;
            $messages = DB::table('messages')
                ->where('hidden_for_user', '<>', $request->user_id)
                ->where('dialog_id', '=', $request->dialog_id)
                ->join('message_contents', 'messages.content_id', '=', 'message_contents.id')
                ->select('messages.dialog_id', 'messages.to_user_id', 'messages.from_user_id', 'messages.created_at', 'message_contents.content', 'messages.id')
                ->skip($start)
                ->take($end)
                ->orderBy('created_at', 'asc')
                ->get();
            $total = DB::table('messages')
                ->where('hidden_for_user', '<>', $request->user_id)
                ->where('dialog_id', '=', $request->dialog_id)
                ->join('message_contents', 'messages.content_id', '=', 'message_contents.id')
                ->select('messages.dialog_id', 'messages.to_user_id', 'messages.from_user_id', 'messages.created_at', 'messages.id', 'message_contents.content')
                ->count();
            $array =[];
            $counter = 0;
            foreach ($messages as $message){
                $files = DB::table('message_files')->select('file', 'file_type')->where('message_id', '=', $message->id)->get()->toArray();

                $array[$counter]['dialog_id'] = $message->dialog_id;
                $array[$counter]['to_user_id'] = $message->to_user_id;
                $array[$counter]['from_user_id'] = $message->from_user_id;
                $array[$counter]['created_at'] = $message->created_at;
                $array[$counter]['content'] = $message->content;
                $array[$counter]['file'] = $files;
                $counter++;
            }
            DB::table('users_badges')->where('dialog_id', '=', $request->dialog_id)->where('user_id', '=', $request->user_id)->update(['unreaded_count'=>0]); // зануляю счетчик баджей
            return response()->json(['status'=>'succes', 'records'=>[
                'messages'=>$array,
                'total'=>$total
            ]]);

        }
    }
    
    public function blockDialog(Request $request)
    {
        if(!isset($request->dialog_id) || !isset($request->user_id) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(!empty(DB::table('blocked_dialogs')->where('dialog_id', '=', $request->dialog_id)->where('blocked_user', '=', $request->user_id)->first())  ) { //проверяю существует ли такая блокировка
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>10,
                'desc'=>'user is already blocked in this dialog'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }elseif(empty(DB::table('dialogs')->where('id', '=', $request->dialog_id)->first())) {
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>5,
                'desc'=>'missing dialog id in DataBase'
            ]]);
        }else{
            DB::table('blocked_dialogs')->insert([
                ['dialog_id' => $request->dialog_id, 'blocked_user' => $request->user_id]
            ]);

            return response()->json(['status'=>'succes']);

        }
    }

    public function getList(Request $request, UsersBadge $badge){
        if(!isset($request->user_id) ){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }elseif(isset($request->search_line)){
            if($request->search_line == ''){
                return response()->json(['status'=>'succes', 'records'=>[
                    'dialogs'=>0,
                    'user_id'=>(int)$request->user_id
                ]]);
            }else{
                $podpiski = Subscribe::select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->get();
                $podpischiki = Subscribe::select('subscriber_id')->where('cur_user_id', '=', $request->user_id)->get();
                $users = [];
                $cur_user = $request->user_id;
                $counter = 0;

                $cur_user = $request->user_id;
                foreach ($podpiski as $item){
                    $users[$counter]['user'] = $item->cur_user_id;
                    $counter++;
                }

                foreach ($podpischiki as $item){
                        foreach ($users as $user){
                            if($item->subscriber_id == $user['user']) continue(2);
                        }
                    $users[$counter]['user'] = $item->subscriber_id;
                    $counter++;
                }


                $counter = 0;
                $filtered_users = [];
                foreach ($users as $user){
                    $item = DB::table('user_notes')->where('user_id', '=', $user['user'])->where('name', 'LIKE', '%'.$request->search_line.'%')->first();
                   if(!empty($item)){
                        $filtered_users[$counter]['user'] = $item->user_id;
                        $counter++;
                   }
                }

                if(empty($filtered_users)) return response()->json(['status'=>'succes', 'records'=>[
                    'dialogs'=>0,
                    'user_id'=>(int)$request->user_id
                ]]);
                $result = [];
                $counter = 0;

                foreach ($filtered_users as $user){
                    $dialog_1 = DB::table('dialogs')->where('user_1', '=', $cur_user)->where('user_2', '=', $user['user'])->where('unvisible_user', '<>', $cur_user)->first();
                    $dialog_2 = DB::table('dialogs')->where('user_1', '=', $user['user'])->where('user_2', '=', $cur_user)->where('unvisible_user', '<>', $cur_user)->first();
                    if(!empty($dialog_1)){
                        $user_info = DB::table('user_notes')->select('name', 'avatar', 'user_id')->where('user_id', '=', $user['user'])->first();
                        $result[$counter]['user']['name'] = $user_info->name;
                        $result[$counter]['user']['avatar'] = $user_info->avatar;
                        $result[$counter]['user']['user_id'] = $user_info->user_id;
                        $result[$counter]['dialog_id'] = $dialog_1->id;
                        $last_message = DB::table('messages')->where('dialog_id', '=', $dialog_1->id)->orderBy('created_at', 'desc')->first();
                        $last_message_id = !empty($last_message) ? $last_message->content_id : 0;
                        $last_message_date = !empty($last_message) ? $last_message->created_at : 0;
                        $last_message_content = ($last_message_id != 0) ? DB::table('message_contents')->where('id', '=', $last_message_id)->first()->content : 0 ;
                        if($last_message_content !== 0){
                            $result[$counter]['last_message'] = $last_message_content;
                            $result[$counter]['last_message_date'] = $last_message_date;
                        }

                    }elseif(!empty($dialog_2)){
                        $user_info = DB::table('user_notes')->select('name', 'avatar', 'user_id')->where('user_id', '=', $user['user'])->first();
                        $result[$counter]['user']['name'] = $user_info->name;
                        $result[$counter]['user']['avatar'] = $user_info->avatar;
                        $result[$counter]['user']['user_id'] = $user_info->user_id;
                        $result[$counter]['dialog_id'] = $dialog_2->id;
                        $last_message = DB::table('messages')->where('dialog_id', '=', $dialog_2->id)->orderBy('created_at', 'desc')->first();
                        $last_message_date = !empty($last_message) ? $last_message->created_at : 0;
                        $last_message_id = !empty($last_message) ? $last_message->content_id : 0;
                        $last_message_content = ($last_message_id != 0) ? DB::table('message_contents')->where('id', '=', $last_message_id)->first()->content : 0 ;
                        if($last_message_content !== 0){
                            $result[$counter]['last_message'] = $last_message_content;
                            $result[$counter]['last_message_date'] = $last_message_date;
                        }


                    }elseif(empty($dialog_1) && empty($dialog_2)){
                        $result[$counter]['user'] = DB::table('user_notes')->select('name', 'avatar', 'user_id')->where('user_id', '=', $user['user'])->get()->toArray();
                        $result[$counter]['dialog_id'] = 0;
                    }
                    $counter++;
                }
                return response()->json(['status'=>'succes', 'records'=>[
                    'dialogs'=>$result,
                    'user_id'=>(int)$request->user_id
                ]]);
            }
        }else{
            $podpiski = Subscribe::select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->get();
            $podpischiki = Subscribe::select('subscriber_id')->where('cur_user_id', '=', $request->user_id)->get();
            $users = [];
            $cur_user = $request->user_id;
            $counter = 0;
            foreach ($podpiski as $item){
                $users[$counter]['user'] = $item->cur_user_id;
                $counter++;
            }
            foreach ($podpischiki as $item){
                foreach ($users as $user){
                    if($item->subscriber_id == $user['user']) continue(2);
                }
                $users[$counter]['user'] = $item->subscriber_id;
                $counter++;
            }
            $result = [];
            $counter = 0;

            foreach ($users as $user){
                $dialog_1 = DB::table('dialogs')->where('user_1', '=', $cur_user)->where('user_2', '=', $user['user'])->where('unvisible_user', '<>', $cur_user)->first();
                $dialog_2 = DB::table('dialogs')->where('user_1', '=', $user['user'])->where('user_2', '=', $cur_user)->where('unvisible_user', '<>', $cur_user)->first();
                if(!empty($dialog_1)){
                    $user_info = DB::table('user_notes')->select('name', 'avatar', 'user_id')->where('user_id', '=', $user['user'])->first();
                    $result[$counter]['user']['name'] = $user_info->name;
                    $result[$counter]['user']['avatar'] = $user_info->avatar;
                    $result[$counter]['user']['user_id'] = $user_info->user_id;
                    $result[$counter]['dialog_id'] = $dialog_1->id;
                    $last_message = DB::table('messages')->where('dialog_id', '=', $dialog_1->id)->orderBy('created_at', 'desc')->first();
                    $last_message_id = !empty($last_message) ? $last_message->content_id : 0;
                    $last_message_content = ($last_message_id != 0) ? DB::table('message_contents')->where('id', '=', $last_message_id)->first()->content : 0 ;
                    $last_message_date = !empty($last_message) ? $last_message->created_at : 0;
                    if($last_message_content !== 0){
                        $result[$counter]['last_message_date'] = $last_message_date;
                        $result[$counter]['last_message'] = $last_message_content;
                    }
                    $result[$counter]['badge'] = $badge->getBadgeByUserAndDialogId($request->user_id, $dialog_1->id);

                }elseif(!empty($dialog_2)){
                    $user_info = DB::table('user_notes')->select('name', 'avatar', 'user_id')->where('user_id', '=', $user['user'])->first();
                    $result[$counter]['user']['name'] = $user_info->name;
                    $result[$counter]['user']['avatar'] = $user_info->avatar;
                    $result[$counter]['user']['user_id'] = $user_info->user_id;
                    $result[$counter]['dialog_id'] = $dialog_2->id;
                    $last_message = DB::table('messages')->where('dialog_id', '=', $dialog_2->id)->orderBy('created_at', 'desc')->first();
                    $last_message_date = !empty($last_message) ? $last_message->created_at : 0;
                    $last_message_id = !empty($last_message) ? $last_message->content_id : 0;
                    $last_message_content = ($last_message_id != 0) ? DB::table('message_contents')->where('id', '=', $last_message_id)->first()->content : 0 ;
                    if($last_message_content !== 0){
                        $result[$counter]['last_message_date'] = $last_message_date;
                        $result[$counter]['last_message'] = $last_message_content;
                    }
                    $result[$counter]['badge'] = $badge->getBadgeByUserAndDialogId($request->user_id, $dialog_2->id);

                }elseif(empty($dialog_1) && empty($dialog_2)){
                    $user_info = DB::table('user_notes')->select('name', 'avatar', 'user_id')->where('user_id', '=', $user['user'])->first();
                    $result[$counter]['user']['name'] = $user_info->name;
                    $result[$counter]['user']['avatar'] = $user_info->avatar;
                    $result[$counter]['user']['user_id'] = $user_info->user_id;
                    $result[$counter]['dialog_id'] = 0;
                }
                $counter++;

            }

            return response()->json(['status'=>'succes', 'records'=>[
                'dialogs'=>$result,
                'user_id'=>(int)$request->user_id
            ]]);

        }
    }

    public function badge(){
        $dialogs = DB::table('dialogs')->get();
        foreach ($dialogs as $dialog){
            UsersBadge::insert(['dialog_id'=>$dialog->id, 'user_id'=>$dialog->user_1, 'unreaded_count'=>0]);
            UsersBadge::insert(['dialog_id'=>$dialog->id, 'user_id'=>$dialog->user_2, 'unreaded_count'=>0]);
        }
    }

}
