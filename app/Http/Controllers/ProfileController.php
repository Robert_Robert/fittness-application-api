<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Models\Comment;
use App\Models\PubDisLike;
use App\Models\PubLike;
use App\Models\Publication;
use App\Models\PublicationAttachUser;
use App\Models\PublicationTag;
use Carbon\Carbon;
use App\User;
use App\Models\UserNote;
use App\Models\Subscribe;
use Illuminate\Http\Response;

class ProfileController extends Controller
{
    public function getProfileById(Request $request){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif( empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }else{
            $subsribers_amount = DB::table('subcribers')->select('subscriber_id')->where('cur_user_id', '=', $request->user_id)->count();
            $subscribs_amount =  DB::table('subcribers')->select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->count();
            $user = DB::table('user_notes')->where('user_id', '=', $request->user_id)->first();
            $user_name = $user->name;
            $avatar = $user->avatar;
            $user_id = $user->user_id;
            $age = $user->age;
            $country = $user->country;
            $city = $user->city;
            $gender = $user->sex;


            $result = [];
            !empty($user_name) ? $result['name']   = $user_name : '';
            !empty($user_id) ?   $result['user_id']   = $user_id : '';
            !empty($avatar)    ? $result['avatar'] = $avatar : '';
            !empty($age)       ? $result['birthDate']    = $age : '';
            !empty($country) ? $result['country']  = $country : '';
            !empty($city) ?    $result['city']     = $city : '';
            !empty($gender) ?    $result['point']     = $gender : '';
            $result['subscribers_amount'] = $subsribers_amount;
            $result['subscribes_amount']  = $subscribs_amount;


            $phisycal_config = DB::table('user_physical_notes')->where('user_id', '=', $request->user_id)->first();
            if(!empty($phisycal_config)){
                ($phisycal_config->weight !== null) ? $result['weight'] = $phisycal_config->weight : '';
                ($phisycal_config->height !== null) ? $result['height'] = $phisycal_config->height : '';
                ($phisycal_config->physical_level !== null) ? $result['physical_level'] = $phisycal_config->physical_level : '';

            }
            $train_recomendation =  $phisycal_config = DB::table('user_train_recomendation')->where('user_id', '=', $request->user_id)->first();
            if(!empty($train_recomendation)){
                ($train_recomendation->train_duration !== null) ? $result['train_time'] = $train_recomendation->train_duration : '';
                ($train_recomendation->place !== null) ? $result['train_place'] = $train_recomendation->place : '';
            }

            !empty( $allergy = DB::table('allergies')->where('user_id', '=', $request->user_id)->first())  ? ($allergy->alergy !== null) ? $result['allergy'] = $allergy->alergy   : '' : '' ;
            !empty( $disease = DB::table('diseases')->where('user_id', '=', $request->user_id)->first())   ? ($disease->disease !== null) ? $result['disease'] = $disease->disease  : '' : '' ;
            !empty( $injuries = DB::table('injuries')->where('user_id', '=', $request->user_id)->first())  ? ($injuries->injury !== null) ? $result['injury'] = $injuries->injury : '' : '' ;

            if(isset($request->curr_user_id ) && $request->user_id == $request->curr_user_id ){
                $subscribes = DB::table('subcribers')->select('cur_user_id')->where('subscriber_id', '=', $request->curr_user_id)->get();
                foreach ($subscribes as $sub){
                    $result['subscribes_ids'][] = $sub->cur_user_id;
                }
            }


            return response()->json(['status'=>'success', 'records'=>[
                'user'=>$result
            ]]);

        }
    }

    public function editProfile(Request $request){
        if(!isset($request->user_id)) {
            return response()->json(['status' => 'error', 'error' => [
                'code' => 8,
                'desc' => 'check parametrs list and their fill'
            ]]);
        }elseif( empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }else{
            if(isset($request->avatar)) {
                $user = DB::table('user_notes')->where('user_id', '=', $request->user_id)->first();
                if(gettype($request->avatar) == 'string'){
                    if ($user->avatar !== null && $request->avatar !== $user->avatar) { //если есть старый аватар то удаляю его
                        if (strpos($user->avatar, 'public') != false) {
                            $str_begin = strpos($user->avatar, 'public');
                            if(file_exists(substr($user->avatar, $str_begin, 100))) unlink(substr($user->avatar, $str_begin, 100));
                        }
                    }
                    DB::table('user_notes')->where('user_id', '=', $request->user_id)->update(['avatar'=>$request->avatar]);
                }else {
                    $filename = uniqid('avatar') . '.' . $request->avatar->getClientOriginalExtension();
                    $path = $request->avatar->move('public/users_files', $filename);
                    if ($user->avatar !== null && $request->avatar !== $user->avatar) { //если есть старый аватар то удаляю его
                        if (strpos($user->avatar, 'public') != false) {
                            $str_begin = strpos($user->avatar, 'public');
                            if(file_exists(substr($user->avatar, $str_begin, 100))) unlink(substr($user->avatar, $str_begin, 100));
                        }
                    }
                    DB::table('user_notes')->where('user_id', '=', $request->user_id)->update(['avatar' => 'http://' . $_SERVER['SERVER_NAME'] . '/' . (string)$path]);
                }
            }
            if(isset($request->name) || isset($request->surname)) {
                $name = $request->name . ' ' . $request->surname;
                DB::table('user_notes')->where('user_id', '=', $request->user_id)->update(['name' => $name]);
                DB::table('users')->where('id', '=', $request->user_id)->update(['name' => $name]);
            }
            if(isset($request->birthDate))DB::table('user_notes')->where('user_id', '=', $request->user_id)->update(['age'=>$request->birthDate]);
            if(isset($request->point))DB::table('user_notes')->where('user_id', '=', $request->user_id)->update(['sex'=>$request->point]);
            if(isset($request->target))DB::table('user_notes')->where('user_id', '=', $request->user_id)->update(['target'=>$request->target]);
            if(isset($request->train_time))DB::table('user_train_recomendation')->where('user_id', '=', $request->user_id)->update(['train_duration'=>$request->train_time]);
            if(isset($request->train_place))DB::table('user_train_recomendation')->where('user_id', '=', $request->user_id)->update(['place'=>$request->train_place]);
            if(isset($request->weight))DB::table('user_physical_notes')->where('user_id', '=', $request->user_id)->update(['weight'=>$request->weight]);
            if(isset($request->height))DB::table('user_physical_notes')->where('user_id', '=', $request->user_id)->update(['height'=>$request->height]);
            if(isset($request->physical_level))DB::table('user_physical_notes')->where('user_id', '=', $request->user_id)->update(['physical_level'=>$request->physical_level]);
            if(isset($request->injury))DB::table('injuries')->where('user_id', '=', $request->user_id)->update(['injury'=>$request->injury]);
            if(isset($request->disease))DB::table('diseases')->where('user_id', '=', $request->user_id)->update(['disease'=>$request->disease]);
            if(isset($request->allergy))DB::table('allergies')->where('user_id', '=', $request->user_id)->update(['alergy'=>$request->allergy]);
            if(isset($request->city))DB::table('user_notes')->where('user_id', '=', $request->user_id)->update(['city'=>$request->city]);


            $subsribers_amount = DB::table('subcribers')->select('subscriber_id')->where('cur_user_id', '=', $request->user_id)->count();
            $subscribs_amount =  DB::table('subcribers')->select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->count();
            $user = DB::table('user_notes')->where('user_id', '=', $request->user_id)->first();
            $user_name = $user->name;
            $avatar = $user->avatar;
            $user_id = $user->user_id;
            $age = $user->age;
            $country = $user->country;
            $city = $user->city;
            $gender = $user->sex;


            $result = [];
            !empty($user_name) ? $result['name']   = $user_name : '';
            !empty($user_id) ?   $result['user_id']   = $user_id : '';
            !empty($avatar)    ? $result['avatar'] = $avatar : '';
            !empty($age)       ? $result['birthDate']    = $age : '';
            !empty($country) ? $result['country']  = $country : '';
            !empty($city) ?    $result['city']     = $city : '';
            !empty($gender) ?    $result['point']     = $gender : '';
            $result['subscribers_amount'] = $subsribers_amount;
            $result['subscribes_amount']  = $subscribs_amount;


            $phisycal_config = DB::table('user_physical_notes')->where('user_id', '=', $request->user_id)->first();
            if(!empty($phisycal_config)){
                ($phisycal_config->weight !== null) ? $result['weight'] = $phisycal_config->weight : '';
                ($phisycal_config->height !== null) ? $result['height'] = $phisycal_config->height : '';
                ($phisycal_config->physical_level !== null) ? $result['physical_level'] = $phisycal_config->physical_level : '';

            }
            $train_recomendation =  $phisycal_config = DB::table('user_train_recomendation')->where('user_id', '=', $request->user_id)->first();
            if(!empty($train_recomendation)){
                ($train_recomendation->train_duration !== null) ? $result['train_time'] = $train_recomendation->train_duration : '';
                ($train_recomendation->place !== null) ? $result['train_place'] = $train_recomendation->place : '';
            }

            !empty( $allergy = DB::table('allergies')->where('user_id', '=', $request->user_id)->first())  ? ($allergy->alergy !== null) ? $result['allergy'] = $allergy->alergy   : '' : '' ;
            !empty( $disease = DB::table('diseases')->where('user_id', '=', $request->user_id)->first())   ? ($disease->disease !== null) ? $result['disease'] = $disease->disease  : '' : '' ;
            !empty( $injuries = DB::table('injuries')->where('user_id', '=', $request->user_id)->first())  ? ($injuries->injury !== null) ? $result['injury'] = $injuries->injury : '' : '' ;

            $subscribes = DB::table('subcribers')->select('cur_user_id')->where('subscriber_id', '=', $request->user_id)->get();
            foreach ($subscribes as $sub){
                $result['subscribes_ids'][] = $sub->cur_user_id;
            }

            return response()->json(['status'=>'success', 'records'=>[
                'user'=>$result
            ]]);
        }
    }

    public function changePassword(Request $request){
        if(!isset($request->user_id) || !isset($request->password) || !isset($request->old_password)) {
            return response()->json(['status' => 'error', 'error' => [
                'code' => 8,
                'desc' => 'check parametrs list and their fill'
            ]]);
        }elseif( empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'user is not found'
            ]]);
        }else{
            $user_obj = User::where('id', '=', $request->user_id)->first();
            if($user_obj->password !== null) {
                if($user_obj->password !== md5($request->old_password)){
                    return response()->json(['status'=>'error', 'error'=>[
                        'code'=>29,
                        'desc'=>'invalid old password'
                    ]]);
                }
                //DB::table('users')->where('id', '=', $request->user_id)->update(['password' => md5($request->password)]);
                $user_obj->password = md5($request->password);
                $user_obj->save();
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error', 'error'=>[
                    'code'=>28,
                    'desc'=>'user don\'t use paswword for login'
                ]]);
            }
        }
    }
}
