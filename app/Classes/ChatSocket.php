<?php

namespace App\Classes\Socket;

use App\Classes\Socket\Base\BaseScoket;
use Ratchet\ConnectionInterface;

class ChatCosket extends BaseScoket{

    protected $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        echo "new connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $numRecv = count($this->clients) - 1;

        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n", $from->resourceId, $msg, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client){
            if($from !== $client){
                //The sender is not reciever, send to each other connected  users
                $client->send($msg);
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error  has  occured: {$e->getMessage()}\n";
        $conn->close();
    }
}