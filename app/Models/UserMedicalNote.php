<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class UserMedicalNotes extends Model
{
    protected $table = 'user_medical_notes';
    protected $fillable = ['note', 'user_id'];
}
