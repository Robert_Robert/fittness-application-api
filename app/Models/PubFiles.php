<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class PubFiles extends Model
{
    protected $table = 'publication_files';
    public $timestamps = false;
    protected $fillable = ['publication_id', 'file', 'file-type'];
}
