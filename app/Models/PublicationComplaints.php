<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class PublicationComplaints extends Model
{
    protected $table = 'publication_complaints';
    public $timestamps = false;
    protected $fillable = ['publication_id', 'user_id', 'reason'];
}
