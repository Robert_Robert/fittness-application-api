<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class PublicationTag extends Model
{
    protected $table = 'publication_tags';
    public $timestamps = false;
    protected $fillable = ['publication_id', 'tag'];
}
