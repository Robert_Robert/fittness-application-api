<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Models\Accessory;
use App\Models\Muscle_group;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Filesystem\Filesystem;


class Simple_exercise extends Model
{
  protected $table = 'simple_exercises';
  public $timestamps = false;
  protected $fillable = ['name', 'image', 'description', 'muscle_group', 'animation', 'exercise_type', 'always_injury', 'newer_injury', 'hidden_exercise', 'load_type', 'date', 'weight', 'user_level', 'exercise_level', 'place', 'sex'];
  public $accessory;

  public function __counsruct(Accessory $accessory){
  	$this->accessory = $accessory;
  }

  public function add_new_execrise(Request $request){

  	if(empty($request->hidden_exercise)) $request->hidden_exercise = 0;
	 	$id = $this->insertGetId([
	 		'name' => $request->name,
	 		'description' => $request->description,
	 		'muscle_group' => $request->muscle_group,
	 		'exercise_type' => $request->exercise_type,
	 		'always_injury' => $request->always_injury,
	 		'newer_injury' => $request->newer_injury,
	 		'weight' => $request->weight,
			'user_level' => $request->user_level,
			'exercise_level' => $request->exercise_level,
			'place' => $request->place,
			'sex' => $request->sex,
	 		'hidden_exercise'  =>  $request->hidden_exercise,
	        'load_type'  =>  $request->load_type,
	        'date'       => date('Y-m-d')
	 		 ]);




	 		if($request->image != null){
		 	 	$filename = uniqid('exercise_').'.'.$request->image->getClientOriginalExtension();
		 	 	$image_path = $request->image->move('public/train_images', $filename);
		 	 	$image_path = str_replace('\\', '/', $image_path);
		 	 	DB::table('simple_exercises')->where('id', '=', $id)->update(['image' => 'http://'.$_SERVER['SERVER_NAME'].'/'.$image_path]);
	 		}

	 		if($request->animation != null){
		 	 	$filename = uniqid('exercise_').'.'.$request->animation->getClientOriginalExtension();
		 	 	$animation_path = $request->animation->move('public/animation', $filename);
		 	 	$animation_path = str_replace('\\', '/', $animation_path);
		 	 	DB::table('simple_exercises')->where('id', '=', $id)->update(['animation' => 'http://'.$_SERVER['SERVER_NAME'].'/'.$animation_path]);
	 		}

	 		foreach ($request->accessory as $accessor) {
				if($accessor != "") Accessory::insert(['simple_exercise_id'=> $id, 'accessory'=>$accessor]);
	 		}


	 		
	 }

  public function edit_execrise(Request $request){
  			if(empty($request->hidden_exercise)) $request->hidden_exercise = 0;

	 		$this->name = $request->name;
	 		$this->description = $request->description;
	 		$this->muscle_group = $request->muscle_group;
	 		$this->exercise_type = $request->exercise_type;
	 		$this->always_injury = $request->always_injury;
	 		$this->newer_injury = $request->newer_injury;
	 		$this->weight = $request->weight;
	  		$this->user_level = $request->user_level;
			$this->exercise_level = $request->exercise_level;
			$this->place = $request->place;
			$this->sex = $request->sex;
	 		$this->hidden_exercise  =  $request->hidden_exercise;
	        $this->load_type  =  $request->load_type;

	        if($request->image != null){
		 	 	$filename = uniqid('exercise_').'.'.$request->image->getClientOriginalExtension();
		 	 	$image_path = $request->image->move('public/train_images', $filename);
		 	 	$image_path = str_replace('\\', '/', $image_path);
				unlink(str_replace('http://188.120.247.45/', '', $this->image));
		 	 	$this->image = 'http://'.$_SERVER['SERVER_NAME'].'/'.$image_path;
	 		}

	 		if($request->animation != null){
		 	 	$filename = uniqid('exercise_').'.'.$request->animation->getClientOriginalExtension();
		 	 	$animation_path = $request->animation->move('public/animation', $filename);
		 	 	$animation_path = str_replace('\\', '/', $animation_path);
				unlink(str_replace('http://188.120.247.45/', '', $this->animation));

		 	 	$this->animation = 'http://'.$_SERVER['SERVER_NAME'].'/'.$animation_path;
	 		}

	 		$this->save();

	 		foreach ($request->accessory as $accessor) {
	 			if(!empty($accessor)) Accessory::insert(['simple_exercise_id'=> $this->id, 'accessory'=>$accessor]);
	 		}
  }	 

  public function delete_exercise(){
  	$this->accessories()->get()->each(function($item, $key){
  		$item->delete();
  	});
  	$this->delete();
  }


  public function serchExercise(Request $request){
  	
  	$result_collection = collect();

  	if(!empty($request->name) && !empty($request->muscle_groups)){

  		$result_collection = DB::table('simple_exercises')->where('name', 'LIKE', '%'.$request->name.'%')->get();

  		foreach ($request->muscle_groups as $group) {
  			$collection = DB::table('simple_exercises')->where('muscle_group', '=', $group)->get();
  		  $result_collection = $result_collection->merge($collection);
  		}
  	} elseif(empty($request->name) && !empty($request->muscle_groups)){
  		foreach ($request->muscle_groups as $group) {
  			$collection = DB::table('simple_exercises')->where('muscle_group', '=', $group)->get();
  			$result_collection = $result_collection->merge($collection);
  		}
  	} elseif(!empty($request->name) && empty($request->muscle_groups)) {
  		$result_collection = DB::table('simple_exercises')->where('name', 'LIKE', '%'.$request->name.'%')->get();
  	}

  	 return $result_collection;
  }	


   public function accessories(){
	 	 return $this->hasMany('App\Models\Accessory', 'simple_exercise_id');
	 } 

}
