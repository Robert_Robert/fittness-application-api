<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class Accessory extends Model
{
  protected $table = 'accessories';
  public $timestamps = false;
  protected $fillable = ['simple_exercise_id', 'accessory'];

   public function simple_exercise()
	{
    		return $this->belongsTo('App\Models\Simple_exercise', 'simple_exercise_id');
	}

}
