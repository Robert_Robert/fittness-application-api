<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Collection;
use App\Models\TrainExercise;
use App\Models\TrainProgram;
use File;

class TrainDay extends Model
{
  protected $table = 'train_days';
  public $timestamps = false;
  protected $fillable = ['train_program_id', 'name', 'image'];

   public function addDay(Request $request, TrainProgram $program){

    $this->train_program_id = $program->id;
    $this->name = $request->name;
       if(isset($request->file)){
           $filename = uniqid('file_').'.'.$request->file->getClientOriginalExtension();
           $path = $request->file->move('public/users_files', $filename);
       }
    $this->image = $path;
    $this->save();

    $day_num = count($request->simple_exercise_id);
    for($count = 0; $count < $day_num; $count++){

           TrainExercise::insert([
              ['train_day_id' => $this->id, 'simple_exercise_id' => $request->simple_exercise_id[$count], 'attempt_numb' =>  $request->attempt_numb[$count], 'weight' => $request->weight[$count], 'repeat_numb' => $request->repeat_numb[$count], 'relax_b_repeat' => $request->relax_b_repeat[$count], 'relax_b_exercise' => $request->relax_b_exercise[$count]]
            ]); 
    }
    
  }

  public function editDay(Request $request, TrainProgram $program){
    $this->trainExercises()->get()->each(function($item, $key){
      $item->delete();
    });

      
    $this->train_program_id = $program->id;
    $this->name = $request->name;
      if(isset($request->file)){
          
          File::delete($this->image);
          $filename = uniqid('file_').'.'.$request->file->getClientOriginalExtension();
          $path = $request->file->move('public/users_files', $filename);
          $this->image = $path;
      }

    $this->save();

    $day_num = count($request->simple_exercise_id);
    for($count = 0; $count < $day_num; $count++){

           TrainExercise::insert([
              ['train_day_id' => $this->id, 'simple_exercise_id' => $request->simple_exercise_id[$count], 'attempt_numb' =>  $request->attempt_numb[$count], 'weight' => $request->weight[$count], 'repeat_numb' => $request->repeat_numb[$count], 'relax_b_repeat' => $request->relax_b_repeat[$count], 'relax_b_exercise' => $request->relax_b_exercise[$count]]
            ]); 
    }
  }

  public function trainExercises(){
	 	 return $this->hasMany('App\Models\TrainExercise', 'train_day_id');
	 }


  public function trainProgram()
	{
    		return $this->belongsTo('App\Models\TrainProgram', 'train_program_id');
	}	 
}
