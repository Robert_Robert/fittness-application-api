<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class disease extends Model
{
    protected $table = 'diseases';
    public $timestamps = false;
    protected $fillable = ['disease', 'user_id'];
}
