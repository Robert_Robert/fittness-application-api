<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class MessageContent extends Model
{
    protected $table = 'message_contents';
    public $timestamps = false;
    protected $fillable = ['content	', 'visible_from', 'visible_to'];

    public function Message(){
        return $this->hasOne('App\Models\Message', 'content_id');
    }
}
