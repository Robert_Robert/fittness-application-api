<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Collection;

class TrainProgram extends Model
{
  protected $table = 'train_programs';
  public $timestamps = false;
  protected $fillable = ['sex', 'age_start', 'age_end', 'imt_type', 'imt_start', 'imt_end', 'target', 'train_num_week', 'train_duration', 'week_amount', 'serial_number', 'cardio', 'strong', 'place'];

  public function search(Request $request){
  	$counter = 0;
	$requests  = DB::table('train_programs');
  	
  	foreach ($_GET as $key => $value) {
		if ($value !== '') {
			if ($key == '_token' || $key == 'imt') {
				continue;
			} elseif ($key == "age_group") {
				$age_array = [];
				if ($request->age_group == '18-45') {
					$age_array[0] = 18;
					$age_array[1] = 45;
				} elseif ($request->age_group == '45-70') {
					$age_array[0] = 45;
					$age_array[1] = 70;
				} else {
					$age_array[0] = 0;
					$age_array[1] = 0;
				}
				$requests = $requests->where('age_start', '=', $age_array[0])->where('age_end', '=', $age_array[1]);
				$counter++;
				continue;
			}
			$requests = $requests->where($key, '=', $value);
			$counter++;
		}
	}
	  $requests = $requests->get();
//	  $result_collection = DB::table('train_programs');
//	foreach($requests as $key => $value){
//		if(empty($key+1)){
//			$result_collection->union($value)->get();
//		}else{
//			$result_collection->union($value);
//		}
//	}
//	  print_r($result_collection);



  	
  	return $requests;
  	
  }

   public function days(){
	 	 return $this->hasMany('App\Models\TrainDay', 'train_program_id');
	 }
}
