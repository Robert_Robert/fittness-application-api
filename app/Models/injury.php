<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class injury extends Model
{
    protected $table = 'injuries';
    public $timestamps = false;
    protected $fillable = ['injury', 'user_id'];
}
