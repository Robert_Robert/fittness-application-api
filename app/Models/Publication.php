<?php

namespace App\Models;

use App\Models\PublicationComplaints;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\PubDisLike;
use DB;
use Illuminate\Http\Response;
use App\Models\Comment;
use Carbon\Carbon;


class Publication extends Model
{
    protected $table = 'publication';
    protected $fillable = ['user_id', 'content', 'created_at', 'updated_at'];
    
    public function searchPublication(Request $request){
    if(!isset($request->page_size) || !isset($request->user_id)){ return response()->json(['status'=>'error', 'error'=>[
        'code'=>8,
        'desc'=>'check parametrs list and their fill'
    ]]);
    }elseif(isset($request->search_type) && $request->search_type == 'tag'){
        $result = [];
        $counter = 0;
        if(isset($request->page_number)) {
            $start = $request->page_number * $request->page_size - $request->page_size;
            $end = $request->page_size;

            $publications = DB::table('publication_tags')
                ->where('publication_tags.tag', 'LIKE', '%' . $request->search_line . '%')
                ->join('publication', 'publication_tags.publication_id', '=', 'publication.id')
                ->select('publication_tags.*', 'publication.*')
                ->orderBy('publication.created_at', 'desc')
                ->skip($start)
                ->take($end)
                ->get();
            $total = DB::table('publication_tags')->where('tag', 'LIKE', '%' . $request->search_line . '%')->join('publication', 'publication_tags.publication_id', '=', 'publication.id')->select('publication_tags.*', 'publication.*')->count();

        }elseif(isset($request->publication_id)){
            $publications = DB::table('publication_tags')
                ->where('publication_tags.tag', 'LIKE', '%' . $request->search_line . '%')
                ->where('publication_tags.publication_id', '<', $request->publication_id)
                ->join('publication', 'publication_tags.publication_id', '=', 'publication.id')
                ->select('publication_tags.*', 'publication.*')
                ->orderBy('publication.created_at', 'desc')
                ->take($request->page_size)
                ->get();
            $total = DB::table('publication_tags')->where('tag', 'LIKE', '%' . $request->search_line . '%')->where('publication_id', '>', $request->publication_id)->join('publication', 'publication_tags.publication_id', '=', 'publication.id')->select('publication_tags.*', 'publication.*')->count();

        }else{
            return response()->json(['status'=>'error', 'error'=>['code'=>8, 'desc'=>'check parametrs list and their fill']]);
        }

            foreach ($publications as $publication){
                $files = DB::table('publication_files')->select('file', 'file_type')->where('publication_id', '=', $publication->id)->get()->toArray();
                $comments_count = DB::table('comments')->where('publication_id', '=', $publication->id)->count();
                $is_liked = !empty(DB::table('pub_like')->where('user_id', '=', $request->auth_user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
                $tags = DB::table('publication_tags')->select('tag')->where('publication_id', '=', $publication->id)->get()->toArray();
                $user_info = DB::table('user_notes')->select('name', 'avatar')->where('user_id', '=', $publication->user_id)->first();

                $like_amount = DB::table('pub_like')->where('publication_id', '=', $publication->publication_id)->count();
                $result[$counter]['publication_id'] = $publication->id;
                $result[$counter]['content'] = $publication->content;
                $result[$counter]['user']['user_id'] = $publication->user_id;
                $result[$counter]['user']['avatar'] = $user_info->avatar;
                $result[$counter]['user']['name'] = $user_info->name;
                $result[$counter]['created_at'] = $publication->created_at;
                $result[$counter]['tags'] = $tags;
                $result[$counter]['comments_count'] = $comments_count;
                $result[$counter]['is_liked'] = $is_liked;
                $result[$counter]['like_amount'] = $like_amount;
                $result[$counter]['files'] = $files;
                $counter++;
            }
            return response()->json(['status'=>'succes', 'records'=>[
                'publication'=>$result,
                'total'=>$total
            ]]);
        }elseif(isset($request->search_type) && $request->search_type == 'name'){
           $result = [];
           $counter = 0;

            if(isset($request->page_number)) {
                $start = $request->page_number * $request->page_size - $request->page_size;
                $end = $request->page_size;

                $publications = DB::table('user_notes')
                    ->where('name', 'LIKE', '%' . $request->search_line . '%')
                    ->join('publication', 'user_notes.user_id', '=', 'publication.user_id')
                    ->select('user_notes.name', 'user_notes.avatar', 'user_notes.user_id', 'publication.content', 'publication.id', 'publication.created_at')
                    ->orderBy('publication.created_at', 'desc')
                    ->skip($start)
                    ->take($end)
                    ->get();
                $total = DB::table('user_notes')->where('name', 'LIKE', '%' . $request->search_line . '%')->join('publication', 'user_notes.user_id', '=', 'publication.user_id')->select('user_notes.name', 'user_notes.avatar', 'user_notes.user_id', 'publication.content', 'publication.id', 'publication.created_at')->count();
            }elseif(isset($request->publication_id)){
                $publications = DB::table('user_notes')
                    ->where('name', 'LIKE', '%' . $request->search_line . '%')
                    ->join('publication', function($join)use($request){
                       $join->on('user_notes.user_id', '=', 'publication.user_id')
                       ->where('publication.id', '<', $request->publication_id);
                    })
                    ->select('user_notes.name', 'user_notes.avatar', 'user_notes.user_id', 'publication.content', 'publication.id', 'publication.created_at')
                    ->orderBy('publication.created_at', 'desc')
                    ->take($request->page_size)
                    ->get();
                $total = DB::table('user_notes')->where('name', 'LIKE', '%' . $request->search_line . '%')->join('publication', function($join)use($request){$join->on('user_notes.user_id', '=', 'publication.user_id')->where('publication.id', '>', $request->publication_id);})->select('user_notes.name', 'user_notes.avatar', 'user_notes.user_id', 'publication.content', 'publication.id', 'publication.created_at')->count();
            }else{
                return response()->json(['status'=>'error', 'error'=>['code'=>8, 'desc'=>'check parametrs list and their fill']]);
            }

            foreach ($publications as $publication){
                $files = DB::table('publication_files')->select('file', 'file_type')->where('publication_id', '=', $publication->id)->get()->toArray();
                $comments_count = DB::table('comments')->where('publication_id', '=', $publication->id)->count();
                $is_liked = !empty(DB::table('pub_like')->where('user_id', '=', $request->auth_user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
                $tags = DB::table('publication_tags')->select('tag')->where('publication_id', '=', $publication->id)->get()->toArray();

                $like_amount = DB::table('pub_like')->where('publication_id', '=', $publication->id)->count();
                $result[$counter]['publication_id'] = $publication->id;
                $result[$counter]['content'] = $publication->content;
                $result[$counter]['user']['user_id'] = $publication->user_id;
                $result[$counter]['user']['avatar'] = $publication->avatar;
                $result[$counter]['user']['name'] = $publication->name;
                $result[$counter]['created_at'] = $publication->created_at;
                $result[$counter]['tags'] = $tags;
                $result[$counter]['comments_count'] = $comments_count;
                $result[$counter]['is_liked'] = $is_liked;
                $result[$counter]['like_amount'] = $like_amount;
                $result[$counter]['files'] = $files;
                $counter++;
            }
            return response()->json(['status'=>'succes', 'records'=>[
                'publication'=>$result,
                'total'=>$total
            ]]);
        }else{
            $result = [];
            $counter = 0;
           if(isset($request->page_number)) {
               $start = $request->page_number * $request->page_size - $request->page_size;
               $end = $request->page_size;
               $publications = DB::table('publication')
                   ->join('user_notes', 'publication.user_id', '=', 'user_notes.user_id')
                   ->select('publication.id', 'publication.user_id', 'publication.content', 'publication.created_at', 'user_notes.avatar', 'user_notes.name')
                   ->orderBy('publication.created_at', 'desc')
                   ->skip($start)
                   ->take($end)
                   ->get();
               $total = DB::table('publication')->join('user_notes', 'publication.user_id', '=', 'user_notes.user_id')->select('publication.id', 'publication.user_id', 'publication.content', 'publication.created_at', 'user_notes.avatar', 'user_notes.name')->count();
           }elseif(isset($request->publication_id)){
               $publications = DB::table('publication')
                   ->where('publication.id', '<', $request->publication_id)
                   ->join('user_notes', 'publication.user_id', '=', 'user_notes.user_id')
                   ->select('publication.id', 'publication.user_id', 'publication.content', 'publication.created_at', 'user_notes.avatar', 'user_notes.name')
                   ->orderBy('publication.created_at', 'desc')
                   ->take($request->page_size)
                   ->get();
               $total =  DB::table('publication')->where('publication.id', '>', $request->publication_id)->join('user_notes', 'publication.user_id', '=', 'user_notes.user_id')->select('publication.id', 'publication.user_id', 'publication.content', 'publication.created_at', 'user_notes.avatar', 'user_notes.name')->count();
           }else{
               return response()->json(['status'=>'error', 'error'=>['code'=>8, 'desc'=>'check parametrs list and their fill']]);
           }
           foreach ($publications as $publication){
               $files = DB::table('publication_files')->select('file', 'file_type')->where('publication_id', '=', $publication->id)->get()->toArray();
               $comments_count = DB::table('comments')->where('publication_id', '=', $publication->id)->count();
               $is_liked = !empty(DB::table('pub_like')->where('user_id', '=', $request->auth_user_id)->where('publication_id', '=', $publication->id)->first()) ? true : false;
               $tags = DB::table('publication_tags')->select('tag')->where('publication_id', '=', $publication->id)->get()->toArray();

               $like_amount = DB::table('pub_like')->where('publication_id', '=', $publication->id)->count();
               $result[$counter]['publication_id'] = $publication->id;
               $result[$counter]['content'] = $publication->content;
               $result[$counter]['user']['user_id'] = $publication->user_id;
               $result[$counter]['user']['avatar'] = $publication->avatar;
               $result[$counter]['user']['name'] = $publication->name;
               $result[$counter]['created_at'] = $publication->created_at;
               $result[$counter]['tags'] = $tags;
               $result[$counter]['comments_count'] = $comments_count;
               $result[$counter]['is_liked'] = $is_liked;
               $result[$counter]['like_amount'] = $like_amount;
               $result[$counter]['files'] = $files;
               $counter++;
           }
           return response()->json(['status'=>'succes', 'records'=>[
               'publication'=>$result,
               'total'=>$total
           ]]);
       }
    }

    public function addComment(Request $request){
        if(!isset($request->user_id) || !isset($request->publication_id) || !isset($request->text)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('publication')->where('id', '=', $request->publication_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>13,
                'desc'=>'this publication does not exist'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'"user is not found'
            ]]);

        }else{
            $dateTime = Carbon::now();
            $dateTime->timezone = "Europe/Kiev";
            $id = Comment::insertGetId(['user_id'=>$request->user_id,
                                  'publication_id'=>$request->publication_id,
                                  'content'=>$request->text,
                                  'created_at'=>$dateTime]);
            return response()->json(['status'=>'success', 'records'=>[
                'comment_id'=>$id
            ]]);
        }
    }
    
    public function editComment(Request $request){
        if(!isset($request->user_id) || !isset($request->publication_id) || !isset($request->text) || !isset($request->comment_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('comments')->where('id', '=', $request->comment_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>16,
                'desc'=>'this comment does not exist'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'"user is not found'
            ]]);

        }else{
            $author_id = DB::table('comments')->where('id', '=', $request->comment_id)->first()->user_id;
            if($author_id != $request->user_id) return response()->json(['status'=>'error', 'error'=>[
                    'code'=>17,
                    'desc'=>'user is not author of comment'
                ]]);
            $dateTime = Carbon::now();
            $dateTime->timezone = "Europe/Kiev";

            Comment::where('id', '=', $request->comment_id)->update([
                'content'=>$request->text,
                'updated_at'=>$dateTime
            ]);
            return response()->json(['status'=>'success', 'records'=>[
                'comment_id'=>$request->comment_id
            ]]);
        }
    }

    public function deleteComment(Request $request){
        if(!isset($request->user_id) || !isset($request->publication_id) || !isset($request->comment_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('comments')->where('id', '=', $request->comment_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>16,
                'desc'=>'this comment does not exist'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'"user is not found'
            ]]);

        }elseif(empty(DB::table('publication')->where('id', '=', $request->publication_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>13,
                'desc'=>'this publication does not exist'
            ]]);
        } else{
            $author_id = DB::table('comments')->where('id', '=', $request->comment_id)->first()->user_id;
            $pub_author_id = DB::table('publication')->where('id', '=', $request->publication_id)->first()->user_id;
            if($author_id != $request->user_id && $pub_author_id != $request->user_id) return response()->json(['status'=>'error', 'error'=>[
                'code'=>17,
                'desc'=>'user is not author of comment'
            ]]);
            $dateTime = Carbon::now();
            $dateTime->timezone = "Europe/Kiev";

            Comment::where('id', '=', $request->comment_id)->delete();
            return response()->json(['status'=>'success']);
        }
    }
    
    public function attachFile(Request $request, $publication_id){
        $mimeTypeImageArray = ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml', 'image/tiff', 'image/vnd.microsoft.icon', 'image/vnd.wap.wbmp', 'image/webp'];

            if(isset($request->file)){
                if(gettype($request->file) == 'array'){
                    $count = 0;
                    $array = [];
                    foreach ($request->file as $item) {
                        $mime = $item->getMimeType();
                        $search = array_search($mime, $mimeTypeImageArray);
                        if($search == false) continue;
                        $filename = uniqid('file_').'.'.$item->getClientOriginalExtension();
                        $path = $item->move('public/users_files', $filename);
                        DB::table('publication_files')->insert(['publication_id'=>$publication_id, 'file'=>'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path, 'file_type'=>$mime]);
                        $array[$count] = 'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path;
                        $count++;
                    }
                }else{
                    $array = [];
                    $mime = $request->file->getMimeType();
                    $filename = uniqid('file_').'.'.$request->file->getClientOriginalExtension();
                    $path = $request->file->move('public/users_files', $filename);
                    DB::table('publication_files')->insert(['publication_id'=>$publication_id, 'file'=>'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path, 'file_type'=>$mime]);
                    $array[] = 'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path;

                }
            }

    }

    public function attachEditFile(Request $request, $publication_id){
        if(isset($request->file)){
            if(gettype($request->file) == 'array'){
                DB::table('publication_files')->where('publication_id', '=', $publication_id)->delete();
                $count = 0;
                $array = [];
                foreach ($request->file as $item) {
                    $mime = $item->getMimeType();
                    $filename = uniqid('file_').'.'.$item->getClientOriginalExtension();
                    $path = $item->move('public/users_files', $filename);
                    DB::table('publication_files')->insert(['publication_id'=>$publication_id, 'file'=>'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path, 'file_type'=>$mime]);
                    $array[$count] = 'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path;
                    $count++;
                }

            }else{
                $mime = $request->file->getMimeType(); $mime = $request->file->getMimeType();
                DB::table('publication_files')->where('publication_id', '=', $publication_id)->delete();
                $array = [];
                $filename = uniqid('file_').'.'.$request->file->getClientOriginalExtension();
                $path = $request->file->move('public/users_files', $filename);
                DB::table('publication_files')->insert(['publication_id'=>$publication_id, 'file'=>'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path, 'file_type'=>$mime]);
                $array[] = 'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path;

            }
        }
    }

    public function disLike(Request $request){
        if(!isset($request->user_id) || !isset($request->publication_id)){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>8,
                'desc'=>'check parametrs list and their fill'
            ]]);
        }elseif(empty(DB::table('publication')->where('id', '=', $request->publication_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>13,
                'desc'=>'this publication does not exist'
            ]]);
        }elseif(empty(DB::table('users')->where('id', '=', $request->user_id)->first())){
            return response()->json(['status'=>'error', 'error'=>[
                'code'=>2,
                'desc'=>'"user is not found'
            ]]);

        }elseif(!empty(DB::table('pub_dislike')->where('user_id', '=', $request->user_id)->where('publication_id', '=', $request->publication_id)->first())) {
            DB::table('pub_dislike')->where('user_id', '=', $request->user_id)->where('publication_id', '=', $request->publication_id)->delete();
            $total = DB::table('pub_dislike')->where('publication_id', '=', $request->publication_id)->count();
            return response()->json(['status'=>'succes', 'records'=>[
                'description'=>'dislike deleted',
                'publication_id'=>(int)$request->publication_id,
                'total'=>(int)$total
            ]]);
        }else{
            PubDisLike::insert(['user_id'=>(int)$request->user_id,
                'publication_id'=>(int)$request->publication_id]);
            $total = DB::table('pub_dislike')->where('publication_id', '=', $request->publication_id)->count();

            return response()->json(['status'=>'success', 'records'=>[
                'total'=>(int)$total,
                'publication_id'=>(int)$request->publication_id
            ]]);
        }
    }

    public function complaintPublication(Request $request, PublicationComplaints $complaint){
        $complaint->publication_id = $request->publication_id;
        $complaint->user_id        = $request->user_id;
        $complaint->reason         = $request->reason;
        $complaint->save();

        return response()->json(['status'=>'success']);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function tags(){
        return $this->hasMany('App\Models\PublicationTag', 'publication_id');
    }

    public function attachUsers(){
        return $this->hasMany('App\Models\PublicationAttachUser', 'publication_id');
    }
    
    
}
