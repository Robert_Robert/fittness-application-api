<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class UserFoodBehaviorRec extends Model
{
    protected $table = 'user_food_behavior_notes';
    protected $fillable = ['note', 'user_id'];
}
