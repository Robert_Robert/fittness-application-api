<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use PushNotification;

class DeviceToken extends Model
{
    protected $table = 'device_tokens';
    public $timestamps = false;
    protected $fillable = ['user_id', 'token'];

    public function getTokensById(int $user_id){
        $tokens_user = $this->select('token')->where('user_id', '=', $user_id)->get();
        $tokens = [];
        $counter = 1;
        foreach ($tokens_user as $item){
            $tokens[$counter] = PushNotification::Device($item->token, array('badge' => $counter));
            $counter++;
        }
        return $tokens;
    }
}
