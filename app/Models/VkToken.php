<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class VkToken extends Model
{
    protected $table = 'vkontakte_tokens';
    protected $fillable = ['user_id', 'token', 'vk_id'];
}
