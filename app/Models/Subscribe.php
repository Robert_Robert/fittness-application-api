<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class Subscribe extends Model
{
    protected $table = 'subcribers';
    protected $fillable = ['cur_user_id', 'subscriber_id'];
}
