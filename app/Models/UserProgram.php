<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Collection;

class UserProgram extends Model
{
    protected $table = 'train_programs';
    public $timestamps = false;
    protected $fillable = ['user_id', 'program_id'];
}
