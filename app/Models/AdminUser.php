<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Validator;
use Illuminate\Support\Facades\Hash;


class AdminUser extends Model
{
    protected $table = 'admin_users';
    public $timestamps = false;
    protected $fillable = ['email', 'password', 'access_level'];

    public function login(String $login, String $password){
        $item = $this->where('email', '=', $login)->first();
        if(empty($item)) return 'empty';
        if (Hash::check($password, $item->password)) {
            return true;
        }else{
            return 'invalid password';
        }
    }

    public function checkMail($email){
        $mail = $this->where('email', '=', $email)->first();
        if(empty($mail)) return false;
        return true;
    }

    public function addNewAdmin($email, $password){
        $this->email = $email;
        $this->password = Hash::make($password);
        $this->save();
        return $this->password;
    }

    public function getHashByLogin(String $login){
        $check = $this->checkMail($login);
        if($check) {
            return $this->where('email', '=', $login)->first()->password;
        } else{
            
        }
    }

    public function checkHash($password){
        if(empty($this->where('password', '=', $password)->first())) return false;
        return true;
    }
}
