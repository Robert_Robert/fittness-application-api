<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class UserTrainRecomendation extends Model
{
    protected $table = 'user_train_recomendation';
    protected $fillable = ['train_duration', 'train_frequency', 'place', 'user_id'];
}
