<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Collection;

class TrainExercise extends Model
{
  protected $table = 'train_exercises';
  public $timestamps = false;
  protected $fillable = ['train_day_id', 'simple_exercise_id', 'attempt_numb', 'weight', 'repeat_numb', 'relax_b_repeat', 'relax_b_exercise'];

  
}
