<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class Message extends Model
{
    protected $table = 'messages';
    protected $fillable = ['dialog_id', 'to_user_id', 'from_user_id', 'content_id', 'hidden_for_user'];

    public function attachFile(Request $request, $mes_id){
        if(isset($request->file)){
            if(gettype($request->file) == 'array'){
                $count = 0;
                $array = [];
                foreach ($request->file as $item) {
                    $mime = $item->getMimeType();
                    $filename = uniqid('file_').'.'.$item->getClientOriginalExtension();
                    $path = $item->move('public/users_files', $filename);
                    DB::table('message_files')->insert(['message_id'=>$mes_id, 'file'=>'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path, 'file_type'=>$mime]);
                    $array[$count] = 'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path;
                    $count++;
                }
            }else{
                $array = [];
                $mime = $request->file->getMimeType();
                $filename = uniqid('file_').'.'.$request->file->getClientOriginalExtension();
                $path = $request->file->move('public/users_files', $filename);
                DB::table('message_files')->insert(['message_id'=>$mes_id, 'file'=>'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path, 'file_type'=>$mime]);
                $array[] = 'http://'.$_SERVER['SERVER_NAME'].'/'.(string)$path;

            }
        }
    }

    public function Dialog()
    {
        return $this->belongsTo('App\Models\Dialog', 'dialog_id');
    }

    public function ToUser()
    {
        return $this->belongsTo('App\User', 'to_user_id');
    }

    public function FromUser()
    {
        return $this->belongsTo('App\User', 'from_user_id');
    }

    public function content()
    {
        return $this->belongsTo('App\Models\MessageContent', 'content_id');
    }


}
