<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class Dialog extends Model
{
    protected $table = 'dialogs';
    public $timestamps = false;
    protected $fillable = ['user_1', 'user_2', 'unvisible_user'];

    

    public function messages(){
        return $this->hasMany('App\Models\Message', 'dialog_id');
    }
}
