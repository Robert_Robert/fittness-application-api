<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class PublicationAttachUser extends Model
{
    protected $table = 'publication_attach_users';
    public $timestamps = false;
    protected $fillable = ['publication_id', 'user_id'];
}
