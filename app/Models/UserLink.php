<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class UserLink extends Model
{
    protected $table = 'user_links';
    protected $fillable = ['user_id', 'link'];
}
