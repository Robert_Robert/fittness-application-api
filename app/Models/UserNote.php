<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use phpDocumentor\Reflection\Types\Self_;

class UserNote extends Model
{
    protected $table = 'user_notes';
    protected $fillable = ['name', 'age', 'sex', 'city', 'target', 'country', 'avatar'];

    public function search(Request $request){
       return self::select('name', 'avatar', 'user_id')->where('name', 'LIKE', '%'.$request->search_line.'%')->get()->toArray();
        
    }
}
