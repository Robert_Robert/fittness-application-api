<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class FbToken extends Model
{
    protected $table = 'facebook_tokens';
    protected $fillable = ['token', 'user_id', 'fb_id'];
}
