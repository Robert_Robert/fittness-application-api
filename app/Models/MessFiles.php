<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class MessFiles extends Model
{
    protected $table = 'message_files';
    public $timestamps = false;
    protected $fillable = ['message_id', 'file', 'file-type'];
}
