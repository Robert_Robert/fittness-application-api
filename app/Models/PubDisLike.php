<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class PubDisLike extends Model
{
    protected $table = 'pub_dislike';
    public $timestamps = false;
    protected $fillable = ['user_id', 'publication_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function publication()
    {
        return $this->belongsTo('App\Models\Publication', 'publication_id');
    }
}
