<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use PushNotification;

class UsersBadge extends Model
{
    protected $table = 'users_badges';
    public $timestamps = false;
    protected $fillable = ['dialog_id', 'user_id', 'unreaded_count'];

    public function addOneBadge($dialog_id, $user_id){
        $badgeModel = $this->where('dialog_id', '=', $dialog_id)->where('user_id', '=', $user_id)->first();
        if(!empty($badgeModel)){
            $unreaded_count = (int)$badgeModel->unreaded_count + 1;
            $badgeModel->unreaded_count = $unreaded_count;
            $badgeModel->save();
        }
    }

    public function getTotalBagde($user_id){
        $dialogs = DB::table('dialogs')->select('id')->where('user_1', '=', $user_id)->orWhere('user_2', '=', $user_id)->get();
        $collection = collect();
        foreach ($dialogs as $dialog){
            !empty($badge = $this->where('dialog_id', '=', $dialog->id)->where('user_id', '=', $user_id)->first()) ?  $collection->push($badge->unreaded_count) : '';
        }
        return $collection->sum();
    }

    public static function deleteBadge($dialog_id){
        DB::table('users_badges')->where('dialog_id', '=', $dialog_id)->delete();
    }

    public function getBadgeByUserAndDialogId($user_id, $dialog_id){
        $badge = DB::table('users_badges')->where('dialog_id', '=', $dialog_id)->where('user_id', '=', $user_id)->first();
        if(!empty($badge)){
            return $badge->unreaded_count;
        }else{
            return 0;
        }
    }
}
