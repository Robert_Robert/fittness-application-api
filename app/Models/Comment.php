<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = ['user_id', 'publication_id', 'content', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function publication()
    {
        return $this->belongsTo('App\Models\Publication', 'publication_id');
    }
}
