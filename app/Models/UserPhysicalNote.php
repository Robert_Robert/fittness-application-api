<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class UserPhysicalNote extends Model
{
    protected $table = 'user_physical_notes';
    protected $fillable = ['weight', 'height', 'physical_level', 'user_id'];
    
    
}
