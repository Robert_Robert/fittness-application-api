<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class BlockedDialog extends Model
{
    protected $table = 'blocked_dialogs';
    public $timestamps = false;
    protected $fillable = ['dialog_id', 'blocked_user'];
}
