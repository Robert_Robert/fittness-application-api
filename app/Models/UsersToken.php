<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;


class UsersToken extends Model
{
    protected $table = 'users_tokens';
    public $timestamps = false;
    protected $fillable = ['user_id', 'token'];
}
