@extends('dashboard.layout')




@section('main-content')
<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_layouts.js') }}"></script>

 @if (count($errors) > 0)
   @foreach ($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
      {{ $error }}
    </div>
   @endforeach 
 @endif 

     
  <!-- Main content -->
   

        <!-- Page header -->
        
         
        <div class="page-header">
                  
        </div>

          <!-- Content area -->
        <div class="content">

        <form class="form-horizontal" action="{{url('/add-new-exercise')}}" method="POST" enctype="multipart/form-data">
           {{ csrf_field() }}
            <div class="panel panel-flat">
              <div class="panel-heading">
                <h5 class="panel-title">Добавить новое упражнение</h5>
                <div class="heading-elements">

                        </div>
              </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset>



                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Название:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="название" name="name" required>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Описание:</label>
                                    <div class="col-lg-9">
                                        <textarea rows="5" cols="5" class="form-control" placeholder="описание" name="description" required></textarea>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-lg-3 control-label" style="margin-top: -10px;">Каритнка:</label>
                                    <div class="col-lg-9" >
                                        <div class="uploader bg-warning"><input type="file" class="file-styled" name="image"><span class="action" style="-webkit-user-select: none;"><i class="icon-googleplus5"></i></span></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Група мышц:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Група мышц" list="mouscle_group" name="muscle_group" required>
                                        <datalist id="mouscle_group">
                                            @foreach($groups as $group)
                                                <option label="{{$group->muscle_group}}"  value="{{$group->muscle_group}}"></option>
                                            @endforeach
                                        </datalist>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Тип упражнения:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="выберете тип упражнения" class="select" name="exercise_type" required>
                                            <option></option>
                                            <option value="на время" >На время</option>
                                            <option value="повторения" >Повторения</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Пол</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Выберете пол" class="select" name="sex" required>
                                            <option></option>
                                            <option value="male" >Мужской</option>
                                            <option value="female" >Женский</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Место</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Выберете место тренировки" class="select" name="place" required>
                                            <option></option>
                                            <option value="home" >Дом</option>
                                            <option value="gym" >Зал</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label" >Вес:</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" placeholder="вес" class="form-control mb-15" name="weight"  required>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label"  style="margin-top: -10px;">Вид нагрузки статическая:</label>
                                    <div class="col-lg-9">
                                        <input type="text" placeholder="" class="form-control" name="load_type" required>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label" style="margin-top: -10px;">Всегда с травмами:</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" placeholder="да/нет" class="form-control mb-15" name="always_injury" required >
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group" style="margin-bottom: 8px;">
                                    <label class="col-lg-3 control-label" style="margin-top: -10px;">Никогда с травмами:</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" placeholder="да/нет" class="form-control mb-15" name="newer_injury"  required>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                                <div class="form-group">
                                    <label class="col-lg-3 control-label" >Видео:</label>
                                    <div class="col-lg-9">
                                        <div class="uploader bg-warning"><input type="file" class="file-styled" name="animation" required><span class="action" style="-webkit-user-select: none;" ><i class="icon-googleplus5"></i></span></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" style="margin-top: -10px;">Скрытое упражнение:</label>
                                    <div class="col-lg-9">
                                        <input type="checkbox" class="switchery-info" style="height: 20px; width: 20px;" name="hidden_exercise" value="1"  >
                                    </div>
                                </div>

                                <div class="form-group" style="margin-bottom: 10px;">
                                    <label class="col-lg-3 control-label" style="margin-top: -10px;">Сложность упражнения</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Выберете сложность" class="select" name="exercise_level" required>
                                            <option></option>
                                            <option value="1" >1</option>
                                            <option value="2" >2</option>
                                            <option value="3" >3</option>
                                            <option value="4" >4</option>
                                            <option value="5" >5</option>
                                            <option value="6" >6</option>
                                            <option value="7" >7</option>
                                            <option value="8" >8</option>
                                            <option value="9" >9</option>
                                            <option value="10" >10</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group" style="margin-bottom: 12px;">
                                    <label class="col-lg-3 control-label" style="margin-top: -10px;">Уровень пользователя</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Выберете уровень пользователя" class="select" name="user_level" required>
                                            <option></option>
                                            <option value="easy"  >Новичок</option>
                                            <option value="medium" >Средний</option>
                                            <option value="hard" >Опытный</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Аксесуары:</label>
                                    <div class="col-lg-9" id="acsesuar_area">



                                        <input type="text" placeholder="аксесуар" class="form-control" name="accessory[]" style=" width: 88%; display: inline-block;">
                                        <button type="button" class="btn bg-pink flex-item " style="float: right; " id="add_acsesuar"><i class="icon-googleplus5"></i></button>
                                    </div>

                                </div>

                            </fieldset>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Добавить<i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
          </form>
          </div>

   <!-- Theme JS files -->
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_layouts.js') }}"></script>
 <!--  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_checkboxes_radios.js') }}"></script>
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/styling/switch.min.js') }}"></script> -->
  <!-- /theme JS files -->






          <!-- Footer -->
          <div class="footer text-muted">
            &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
          </div>
          <!-- /footer -->

        </div>
        <!-- /content area -->    
        
      
@stop