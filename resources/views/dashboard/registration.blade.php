@extends('dashboard.layout')




@section('main-content')



    <div class="content login_form">
        <h3 style="color: green;">{{ session('message') }}</h3>

        <!-- Advanced login -->
        <form action="{{url('/new-admin')}}" method="POST" name="refistration">
            {{ csrf_field() }}
            <div class="panel panel-body login-form">
                <div class="text-center">
                    <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                    <h5 class="content-group">Добавить администратора <small class="display-block"></small></h5>
                </div>

                <div class="content-divider text-muted form-group"><span>Ваши данные</span></div>

                <div class="form-group has-feedback has-feedback-left">
                    <input type="text" class="form-control" placeholder="email" required name="login" value="{{old('login')}}">
                    <div class="form-control-feedback">
                        <i class="icon-user-check text-muted"></i>
                    </div>
                    {{--<span class="help-block text-danger"><i class="icon-cancel-circle2 position-left"></i> This username is already taken</span>--}}
                </div>

                <div class="form-group has-feedback has-feedback-left">
                    <input type="password" class="form-control" placeholder="password" required name="password" value="{{old('password')}}">
                    <div class="form-control-feedback">
                        <i class="icon-user-lock text-muted"></i>
                    </div>
                </div>

                <div class="form-group has-feedback has-feedback-left">
                    <input type="password" class="form-control" placeholder="repeat password" required name="repeat_password" value="{{old('repeat_password')}}">
                    <div class="form-control-feedback">
                        <i class="icon-user-lock text-muted"></i>
                    </div>
                </div>

                {{--<div class="content-divider text-muted form-group"><span>Your privacy</span></div>--}}

                {{--<div class="form-group has-feedback has-feedback-left">--}}
                    {{--<input type="text" class="form-control" placeholder="Your email">--}}
                    {{--<div class="form-control-feedback">--}}
                        {{--<i class="icon-mention text-muted"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="form-group has-feedback has-feedback-left">--}}
                    {{--<input type="text" class="form-control" placeholder="Repeat email">--}}
                    {{--<div class="form-control-feedback">--}}
                        {{--<i class="icon-mention text-muted"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="content-divider text-muted form-group"><span>Additions</span></div>--}}

                {{--<div class="form-group">--}}
                    {{--<div class="checkbox">--}}
                        {{--<label>--}}
                            {{--<input type="checkbox" class="styled" checked="checked">--}}
                            {{--Send me <a href="#">test account settings</a>--}}
                        {{--</label>--}}
                    {{--</div>--}}

                    {{--<div class="checkbox">--}}
                        {{--<label>--}}
                            {{--<input type="checkbox" class="styled" checked="checked">--}}
                            {{--Subscribe to monthly newsletter--}}
                        {{--</label>--}}
                    {{--</div>--}}

                    {{--<div class="checkbox">--}}
                        {{--<label>--}}
                            {{--<input type="checkbox" class="styled">--}}
                            {{--Accept <a href="#">terms of service</a>--}}
                        {{--</label>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <button type="submit" class="btn bg-teal btn-block btn-lg">Зарегистрировать <i class="icon-circle-right2 position-right"></i></button>
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <span class="help-block text-danger"><i class="icon-cancel-circle2 position-left"></i>{{ $error }}</span>
                    @endforeach
                @endif
            </div>
        </form>



        <!-- /advanced login -->




    </div>










            <!-- /content area -->
            <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
            <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_multiselect.js') }}"></script>





@stop