<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FitnessApp</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('resources/assets/admin/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('resources/assets/admin/assets/css/minified/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('resources/assets/admin/assets/css/minified/core.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('resources/assets/admin/assets/css/minified/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('resources/assets/admin/assets/css/minified/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('resources/assets/admin/assets/css/additional.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->



    <!-- multiselect -->
    <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/notifications/pnotify.min.js') }}"></script> -->


    <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/core/app.js') }}"></script>


    <!-- /theme JS files -->



    <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/main.js') }}"></script>
</head>

<body class="pace-done"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div></div>

<!-- Main navbar -->

<!-- /main navbar -->


<!-- Page container -->
<div class="page-container" style="min-height:266px">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->

        <!-- /main sidebar -->

        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content login_form">

                <!-- Simple login form -->
                <form action="{{url('/admin-login')}}" method="POST" name="login_form">
                    {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                            <h5 class="content-group">Войдите в свою учетную запись <small class="display-block">Введите ваши данные</small></h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" placeholder="Логин" name="login" required >
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" placeholder="Пароль" required name="password">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Войти <i class="icon-circle-right2 position-right"></i></button>
                        </div>


                    </div>
                </form>
                <!-- /simple login form -->

                @section('content')
                    <h3 style="color: green;">{{ session('message') }}</h3>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


            </div>

        </div>

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->





</body>


</html>
