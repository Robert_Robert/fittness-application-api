@extends('dashboard.layout')




@section('main-content')

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <!-- Main content -->

        <!-- Content area -->
        <div class="content">
    

        <div class="panel panel-white">
            <div class="panel-heading">
              <input type="hidden" name="_method" value="DELETE" >
              <!-- Default multiselect -->
            

               <div class="col-md-12">
                 
                   <form action="{{url('/search-program')}}" method="GET" name="program-form">                  
                       {{ csrf_field() }}
                    <fieldset>
                              <legend class="text-semibold"><i class="fa fa-heartbeat"></i>Поля для фильтрации<a href="{{url('/add-new-program')}}"><button type="button" class="btn bg-pink " style="float: right; margin-top: -10px;"><i class="icon-googleplus5"></i></button></a> </legend>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Пол:</label>
                            
                            <select name="sex" class="form-control">
                                <option></option>
                              <option value="male">Мужской</option>
                              <option value="female">Женский</option>
                            </select>
                          </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Возрастная група:</label>
                                <select name="age_group" class="form-control">
                                    <option></option>
                                    <option value="18-45" @if(isset($program) && $program->age_start == 18) selected @endif >18 - 45 лет</option>
                                    <option value="45-70"  @if(isset($program) && $program->age_start == 45) selected @endif >45 - 70 лет</option>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Индекс масы тела (ИМТ):</label>
                            <input type="text"  class="form-control" name="imt" >
                          </div>
                        </div>



                          <div class="col-md-4">
                              <div class="form-group">
                                  <label>Цель:</label>
                                  <select name="target" class="form-control">
                                      <option></option>
                                      <option value="restoration"  >Восстановление</option>
                                      <option value="back"  >Спина</option>
                                      <option value="legs" >Ноги</option>
                                      <option value="relax"  >Расслабление</option>
                                      <option value="set_weight"  >Набор массы</option>
                                      <option value="slim"  >Похудение</option>
                                      <option value="set_muscles"  >Набор мышц</option>
                                  </select>
                              </div>
                          </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Кол-во занятий в нелелю:</label>
                            <input type="text"  class="form-control" name="train_num_week" >
                          </div>
                        </div>


                     <div class="col-md-4">
                      <div class="form-group">
                      <label>Продолжительность занятия:</label>
                        <div class="input-group">
                        <input type="text" class="form-control" name="train_duration">
                        <span class="input-group-btn">
                          <button class="btn bg-teal" type="submit">Фильтрация</button>
                        </span>
                      </div>
                      </div>
                    </div>

                      </div>

                    </fieldset>
                  </div>
              
          

            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer"><div class="datatable-scroll-lg"><table class="table tasks-list table-lg dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
              <thead>
              <tr role="row">
                
                <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 40%;">
                Пол</th>

                <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 20%;">
                Возрастная группа</th>

                <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 15%;">
                Цель</th>

                <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 15%;">
               ИМТ</th>

                 

              </tr>
              </thead>
              <tbody>
                
               @foreach($programs as $program) 
              <tr role="row" class="odd">
                  
                  
                          <td>
                            <div class="text-semibold"><a href="{{url('edit-program/'.$program->id)}}"> @if($program->sex == 'male')
                                        Мужской
                                    @elseif($program->sex == 'female')
                                        Женский
                                    @endif</a></div>
                            <!-- <div class="text-muted" style="max-height: 20px; overflow: hidden;">@{{$exercise->description}}</div> -->
                          </td>
                          <td>
                              @if($program->age_start == '18')
                                  18 - 45
                              @elseif($program->age_start == '45')
                                  45 - 70
                              @endif
                          </td>
                          <td>
                              @if($program->target == 'restoration')
                                  Восстановление
                              @elseif($program->target == 'back')
                                  Спина
                              @elseif($program->target == 'legs')
                                  Ноги
                              @elseif($program->target == 'relax')
                                  Расслабление
                              @elseif($program->target == 'set_weight')
                                  Набор массы
                              @elseif($program->target == 'slim')
                                  Похудение
                              @elseif($program->target == 'set_muscles')
                                  Набор мышц
                              @endif
                          </td>
                          <td>
                              {{$program->imt_start }} - {{$program->imt_end }}
                          </td>
                         
        
                      </tr>
              @endforeach        
                     
                     </tbody>
            </table></div></div>
          </div>

          <!-- Main charts -->
          
          <!-- /main charts -->


          <!-- Dashboard content -->
          
          <!-- /dashboard content -->


          <!-- Footer -->
         

        </div>
        <!-- /content area -->
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_multiselect.js') }}"></script>
  
     
  


@stop