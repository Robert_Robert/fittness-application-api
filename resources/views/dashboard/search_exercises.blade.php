@extends('dashboard.layout')




@section('main-content')

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <!-- Main content -->

        <!-- Page header -->
        
         
        <div class="page-header">
               <h3 style="color: green;">{{ session('message') }}</h3>   
        </div>
        
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

        <div class="panel panel-white">
            <div class="panel-heading">
              <input type="hidden" name="_method" value="DELETE" >
              <!-- Default multiselect -->
              <form action="{{url('/search-exercises')}}" method="GET">
              {{ csrf_field() }}
                <div class="flex-container">

                    <div class="form-group flex-item">
                    
                      <div class="multi-select-full">
                        <select class="multiselect-filtering" multiple="multiple" name="muscle_groups[]">
                        @foreach($groups as $group)
                          <option value="{{$group->name}}">{{$group->name}}</option>
                        @endforeach  
                        </select>
                      </div>
                    </div>
                <!-- /default multiselect -->

                <div class="form-group flex-item">
                      
                      <div class="col-lg-10">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="Teal button" name="name">
                          <span class="input-group-btn">
                            <button class="btn bg-teal" type="submit">Искать</button>
                          </span>
                        </div>
                      </div>
                    </div>
                    <a href="{{url('/add-simple-exercise')}}"><button type="button" class="btn bg-pink flex-item" style="margin-bottom: 20px;"><i class="icon-googleplus5"></i></button></a>                </div>

              </div>  
              </form> 
              
          

            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer"><div class="datatable-scroll-lg"><table class="table tasks-list table-lg dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
              <thead>
              <tr role="row">
                <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 20px;">
                #</th>

                <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 40%;">
                Название и описание</th>

                <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 20%;">
                Група мышц</th>

                <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 15%;">
                Дата</th>

                <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 15%;">
                Публикация</th>

                 <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 15%;">
                </th>

              </tr>
              </thead>
              <tbody>
                
               @foreach($exercises as $exercise) 
              <tr role="row" class="odd">
                  <td class="sorting_1">#{{$exercise->id}}</td>
                  
                          <td>
                            <div class="text-semibold"><a href="{{url('edit-simple-exercise/'.$exercise->id)}}">{{$exercise->name}}</a></div>
                            <div class="text-muted" style="max-height: 20px; overflow: hidden;">{{$exercise->description}}</div>
                          </td>
                          <td>
                           {{$exercise->muscle_group}}
                          </td>
                          <td>
                            <div class="input-group input-group-transparent">
                              <div class="input-group-addon"><i class="icon-calendar2 position-left"></i></div>
                              {{$exercise->date}}
                            </div>
                          </td>
                          <td>
                           @if($exercise->hidden_exercise == 1)
                              Нет
                           @else
                              Да
                           @endif   
                          </td>
                          <td>
                            <button type="button" class="btn bg-danger flex-item delete-exercise" style="margin-bottom: 20px;" data-exercise="{{$exercise->id}}"><i class="icon-cross2" ></i></button>
                          </td>
        
                      </tr>
              @endforeach        
                     
                     </tbody>
            </table></div></div>
          </div>

          <!-- Main charts -->
          
          <!-- /main charts -->


          <!-- Dashboard content -->
          
          <!-- /dashboard content -->


          <!-- Footer -->
         

        </div>
        <!-- /content area -->
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_multiselect.js') }}"></script>
  
     
  


@stop