@extends('dashboard.layout')




@section('main-content')
 <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
 <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_bootstrap_select.js') }}"></script>


  
<input type="hidden" name="_method" value="DELETE">
 @if (count($errors) > 0)
   @foreach ($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
      {{ $error }}
    </div>
   @endforeach 
 @endif 

     
  <!-- Main content -->
   

        <!-- Page header -->
        
         
        <div class="page-header">


        @if(session('notice') !== null)

            <div class="alert alert-danger no-border">
              <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Закрыть</span></button>
              <span class="text-semibold">{{ session('notice') }}</span> 
            </div>

        @endif    

        </div>

          <!-- Content area -->
        <div class="content">
            
        
            <div class="panel panel-flat">
              <div class="panel-heading">
                <h5 class="panel-title">Добавление програмы тренировки</h5>
               
              <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

              <div class="panel-body">
                <div class="row">
                  
      
               
                  <div class="col-md-12">
                  @if(isset($program->id))
                   <form action="{{url('/edit-program/'.$program->id)}}" method="POST" name="program-form">
                  @else
                   <form action="{{url('/add-program')}}" method="POST" name="program-form">                  @endif 
                       {{ csrf_field() }}
                    <fieldset>
                              <legend class="text-semibold"><i class="fa fa-heartbeat"></i>Необходимые для заплонения поля</legend>

                      <div class="row">
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>Пол:</label>
                          <!--   <input type="text"  class="form-control" name="sex" required  @if(isset($program)) value="{{$program->sex}}" @endif  > -->
                          <select name="sex" class="form-control">
                              <option value="male" @if(isset($program) && $program->sex == 'male') selected @endif >Мужской</option>
                              <option value="female"  @if(isset($program) && $program->sex == 'female') selected @endif >Женский</option>
                          </select>
                          </div>
                        </div>

                        <div class="col-md-2">
                          <div class="form-group">
                            <label>Возрастная група:</label>
                              <select name="age_group" class="form-control">
                                  <option value="18-45" @if(isset($program) && $program->age_start == 18) selected @endif >18 - 45 лет</option>
                                  <option value="45-70"  @if(isset($program) && $program->age_start == 45) selected @endif >45 - 70 лет</option>
                              </select>
                          </div>
                        </div>

                          <div class="col-md-2">
                              <div class="form-group">
                                  <label>Цель:</label>
                                  <select name="target" class="form-control" required>
                                      <option value="restoration" @if(isset($program) && $program->target == 'restoration') selected @endif >Восстановление</option>
                                      <option value="back" @if(isset($program) && $program->target == 'back') selected @endif >Спина</option>
                                      <option value="legs" @if(isset($program) && $program->target == 'legs') selected @endif >Ноги</option>
                                      <option value="relax" @if(isset($program) && $program->target == 'relax') selected @endif >Расслабление</option>
                                      <option value="set_weight" @if(isset($program) && $program->target == 'set_weight') selected @endif >Набор массы</option>
                                      <option value="slim" @if(isset($program) && $program->target == 'slim') selected @endif >Похудение</option>
                                      <option value="set_muscles" @if(isset($program) && $program->target == 'set_muscles') selected @endif >Набор мышц</option>
                                  </select>
                              </div>
                          </div>

                        <div class="col-md-3" >
                          <div class="form-group">
                            <label>Диапазон индекса масы тела (ИМТ):</label>
                              <select name="imt" class="form-control" style="font-size: 110%;" id="imt" required>
                                  <option></option>
                                  <option value="15-21.9" @if(isset($program) && $program->imt_start == 15) selected @endif >15 - 21.9</option>
                                  <option value="22-26"  @if(isset($program) && $program->imt_start == 22) selected @endif >22 - 26 </option>
                                  <option value="27-30"  @if(isset($program) && $program->imt_start == 27) selected @endif >27 - 30 </option>
                                  <option value="31-100"  @if(isset($program) && $program->imt_start == 31) selected @endif >31 - &#8734; </option>
                              </select>
                          </div>
                        </div>


                        <div class="col-md-1" style="padding: 0 0 ;">
                            <div class="form-group">
                                <label>Тип ИМТ:</label>
                                <select  class="form-control" id="imt_type" disabled >
                                    @if(isset($program->id))
                                        <option selected>{{$program->imt_type}}</option>
                                    @endif
                                </select>
                                @if(isset($program->id))
                                    <input type="hidden" name="imt_type" id="imt_type_value" value="{{$program->imt_type}}">
                                @else
                                    <input type="hidden" name="imt_type" id="imt_type_value">
                                @endif
                            </div>
                        </div>




                          <div class="col-md-2">
                              <div class="form-group">
                                  <label>Место:</label>
                                  <select name="place" class="form-control">
                                      <option value="gym" @if(isset($program) && $program->place == 'gym') selected @endif >Зал</option>
                                      <option value="home" @if(isset($program) && $program->place == 'home') selected @endif >Дом</option>
                                  </select>
                              </div>
                          </div>


                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Кол-во занятий в нелелю:</label>
                            <input type="text"  class="form-control" name="train_num_week" required @if(isset($program)) value="{{$program->train_num_week}}" @endif>
                          </div>
                        </div>


                        <div class="col-md-2">
                          <div class="form-group">
                            <label>Продолжительность:</label>
                            <input type="text"  class="form-control" name="train_duration" required @if(isset($program)) value="{{$program->train_duration}}" @endif>
                          </div>
                        </div>


                        <div class="col-md-2">
                              <div class="form-group">
                                  <label>Кол-во недель:</label>
                                  <input type="text"  class="form-control" name="week_amount" required @if(isset($program)) value="{{$program->week_amount}}" @endif>
                              </div>
                        </div>


                          <div class="col-md-2">
                              <div class="form-group">
                                  <label>Номер программы:</label>
                                  <input type="text"  pattern="\d*" placeholder="только цифры" class="form-control" name="serial_number" required @if(isset($program)) value="{{$program->serial_number}}" @endif>
                              </div>
                          </div>


                          <div class="col-md-1">
                              <div class="form-group">
                                  <label style="display: block;">Кардио:</label>
                                  <div class="col-lg-9">
                                      <input type="checkbox" class="switchery-info" style="height: 20px; width: 20px;" name="cardio" value="1" @if(isset($program)) @if($program->cardio == 1) checked @endif @endif>
                                  </div>
                              </div>
                          </div>


                          <div class="col-md-1">
                              <div class="form-group">
                                  <label style="display: block;">Силовая:</label>
                                  <div class="col-lg-9">
                                      <input type="checkbox" class="switchery-info" style="height: 20px; width: 20px;" name="strong" value="1" @if(isset($program)) @if($program->strong == 1) checked @endif @endif>
                                  </div>
                              </div>
                          </div>

                      </div>

                    </fieldset>
                  </div>
                  <div class="text-right">
                     <button type="submit" class="btn btn-primary">Добавить</button>
                  </div>
                  </form>

    
                  <div class="col-md-12">
                    <fieldset>
                              <legend class="text-semibold"><i class="fa fa-heartbeat"></i>Тренировочные дни </legend>

                      <div class="row" id="train_days_block">


                            <div class="col-md-12">
                            <form action="@if(isset($program)){{url('/add-train-day/'.$program->id)}}@endif" method="POST" name="program-form" enctype="multipart/form-data">
                            {{ csrf_field() }}    

                                    <!-- Accordion with different panel styling -->
                     
                            <div class="panel-group" id="accordion-styled">
                              <div class="panel">
                                <div class="panel-heading bg-teal">
                                  <h6 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="#accordion-styled-group999">Тренировочный день </a>
                                    <button type="button" class="btn btn-primary btn-rounded add-exercise" style="position: absolute;
                                    right: 0px; top: -8px;"><i class="icon-plus-circle2"></i> Упражнение</button>
                                  </h6>
                                </div>
                                <div id="accordion-styled-group999" class="panel-collapse collapse">
                                  <div class="panel-body exercise-container">

                                     
                                         
                                              <div class="col-md-12">
                                                <div class="panel-group panel-group-control panel-group-control-right content-group-lg">
                                                 

                                                  <div class="panel panel-white">
                                                    <div class="panel-heading" style="background: rgba(196, 225, 255, 0.55);">
                                                      <h6 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse" href="#collapsible-control-right-group0">Упражнение</a>
                                                      </h6>
                                                    </div>
                                                    <div id="collapsible-control-right-group0" class="panel-collapse collapse">
                                                      <div class="panel-body">

                                                          <div class="col-md-6">
                                                        
                                                             <div class="col-md-12">
                                                              <div class="form-group"> <label>Выберете упражнение</label> 

                                                             <input type="text" placeholder="начните вводить название" class="form-control" list="exercise_search" required name="simple_exercise_id[]" autocomplete="off">
                                                             <datalist id="exercise_search">

                                                               @foreach($exercises as $exercise)
                                                                          <option label="{{$exercise->name}}"  value="{{$exercise->id}}"></option>
                                                               @endforeach
                                                             </datalist>
                                                             </div> 
                                                            </div> 

                                                            <!-- Live search support -->
                 


                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Количество подходов:</label>
                                                                <input type="text"  class="form-control" required name="attempt_numb[]">
                                                              </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Вес:</label>
                                                                <input type="text"  class="form-control" required name="weight[]">
                                                              </div>
                                                            </div>

                                                           </div> 
                                                           <div class="col-md-6">

                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Кол-во повторений за подход:</label>
                                                                <input type="text" class="form-control" required name="repeat_numb[]">
                                                              </div>
                                                            </div>

                                                           
                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Отдых между подходами:</label>
                                                                <input type="text"  class="form-control" required name="relax_b_repeat[]">
                                                              </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Отдых между упражнениями:</label>
                                                                <input type="text"  class="form-control" required name="relax_b_exercise[]">
                                                              </div>
                                                            </div>

                                                           </div> 

                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                </div>

                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label>Название:</label>
                                                      <input type="text"  class="form-control" required name="name">
                                                    </div>
                                                  </div>

                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label>Картинка:</label>
                                                  <div class="uploader bg-warning"><div class="uploader bg-warning"><input type="file" class="file-styled" required name="file"><span class="filename" style="user-select: none; display: none;">No file selected</span><span class="action" style="user-select: none;"><i class="icon-googleplus5"></i></span></div><span class="action" style="-webkit-user-select: none;"><i class="icon-googleplus5"></i></span></div>

                                          </div>
                                      </div>

                                      <div class="col-md-12">
                                            <div class="form-group">
                                               <button type="submit" class="btn bg-teal-400 btn-labeled" style=" display: block; margin-left: auto; margin-right: auto;"><b><i class="icon-add" ></i></b>Добавить</button>
                                            </div>
                                      </div>

                                  </div>
                                </div>
                              </div>
                            </div>

                            </form>
                      </div>

                    @if(isset($train_days))
                      @foreach($train_days as $day)
                        <div class="col-md-12 program-day">
                            <form action="{{url('/edit-train-day/'.$program->id.'/'.$day["id"])}}" method="POST" name="program-form" enctype="multipart/form-data">
                            {{ csrf_field() }}    
                            <div class="panel-group" id="accordion-styled">
                              <div class="panel">
                                <div class="panel-heading bg-teal" style="background: #2196f3; border-color: #2196f3;">
                                  <h6 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion-styled" href="{{'#accordion-styled-group'.$day['id']}}">Тренировочный день </a>
                                    <button type="button" class="btn btn-primary btn-rounded add-exercise" style="position: absolute; right: 0px; top: -8px; background: #009688; border-color: #009688;"><i class="icon-plus-circle2"></i> Упражнение</button>
                                  </h6>
                                </div>
                                <div id="{{'accordion-styled-group'.$day['id']}}" class="panel-collapse collapse">
                                  <div class="panel-body exercise-container">


                                     @foreach($day['exercises'] as $train)
                                         
                                              <div class="col-md-12">
                                                <div class="panel-group panel-group-control panel-group-control-right content-group-lg">
                                                 

                                                  <div class="panel panel-white">
                                                    <div class="panel-heading" style="background: rgba(196, 225, 255, 0.55);">
                                                      <h6 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse" href="{{'#collapsible-control-right-group'.$train['id']}}">Упражнение</a>
                                                      </h6>
                                                    </div>
                                                    <div id="{{'collapsible-control-right-group'.$train['id']}}" class="panel-collapse collapse">
                                                      <div class="panel-body">

                                                           <div class="col-md-6">
                                                        
                                                             <div class="col-md-12">
                                                              <div class="form-group"> <label>Выберете упражнение</label> 
                                                             <input type="text" placeholder="начните вводить название" class="form-control" list="exercise_search" required name="simple_exercise_id[]" value="{{$train['simple_exercise_id']}}" autocomplete="off">
                                                             <datalist id="exercise_search">

                                                               @foreach($exercises as $exercise)
                                                                          <option label="{{$exercise->name}}"  value="{{$exercise->id}}"></option>
                                                               @endforeach
                                                             </datalist>
                                                             </div> 
                                                            </div> 

                                                            <!-- Live search support -->
                 


                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Количество подходов:</label>
                                                                <input type="text"  class="form-control" required name="attempt_numb[]" value="{{$train['attempt_numb']}}">
                                                              </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Вес:</label>
                                                                <input type="text"  class="form-control" required name="weight[]" value="{{$train['weight']}}">
                                                              </div>
                                                            </div>

                                                          </div>
                                                          <div class="col-md-6">  

                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Кол-во повторений за подход:</label>
                                                                <input type="text" class="form-control" required name="repeat_numb[]" value="{{$train['repeat_numb']}}">
                                                              </div>
                                                            </div>

                                                           
                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Отдых между подходами:</label>
                                                                <input type="text"  class="form-control" required name="relax_b_repeat[]" value="{{$train['relax_b_repeat']}}">
                                                              </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                              <div class="form-group">
                                                                <label>Отдых между упражнениями:</label>
                                                                <input type="text"  class="form-control" required name="relax_b_exercise[]" value="{{$train['relax_b_exercise']}}">
                                                              </div>
                                                            </div>
                                                          </div>  

                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                </div>
                                          @endforeach      

                                                

                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                      <label>Название:</label>
                                                      <input type="text"  class="form-control" required name="name" value="{{$day['name']}}">
                                                    </div>
                                                  </div>


                                                 <div class="col-md-4">
                                                     <div class="panel-body">
                                                         <div class="thumbnail" style="width: 100px;margin-left: auto;margin-right: auto;margin-top: -30px;">
                                                             <div class="thumb">
                                                                 <a href="{{ asset($day['image']) }}" data-popup="lightbox">
                                                                     <img src="{{ asset($day['image']) }}" alt="" style="height: 100px;">
                                                                     <span class="zoom-image"><i class="icon-plus2"></i></span>
                                                                 </a>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>


                                                 <div class="col-md-4">
                                                     <div class="form-group">
                                                         <label>Изменить картинку:</label>
                                                         <div class="uploader bg-warning"><div class="uploader bg-warning"><input type="file" class="file-styled" name="file"><span class="filename" style="user-select: none; display: none;">No file selected</span><span class="action" style="user-select: none;"><i class="icon-googleplus5"></i></span></div><span class="action" style="-webkit-user-select: none;"><i class="icon-googleplus5"></i></span></div>

                                                     </div>
                                                 </div>





                                                 <div style="clear: both"></div>

                                                  

                                                  <button type="submit" class="btn btn-primary btn-rounded"><i class="icon-pencil"></i> Редактировать</button>

                                                  <button type="button" class="btn btn-danger btn-rounded delete-day" data-day="{{$day['id']}}" style="float: right;"><i class="icon-close2"></i> Удалить</button>

                                  </div>
                                </div>
                              </div>
                            </div>

                            </form>
                          </div>
                        @endforeach
                      @endif  



                      

  
                    @if(!isset($program->id))
                    <div class="hide_train_days"> </div>
                    @endif
                  </div>

                  </div>

              </div>
            </div>
                
        </div>

   <!-- Theme JS files -->
            <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/components_thumbnails.js') }}"></script>
            <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/media/fancybox.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_layouts.js') }}"></script>
 <!--  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script> -->
 <!--  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_select2.js') }}"></script> -->

  





        </div>
        <!-- /content area -->    
  <div class="new-exercise" style="display: none;"><div class="col-md-12"> <div class="panel-group panel-group-control panel-group-control-right content-group-lg"> <div class="panel panel-white"> <div class="panel-heading" style="background: rgba(196, 225, 255, 0.55);"> <h6 class="panel-title"> <a class="collapsed" data-toggle="collapse" href="#collapsible-control-right-group0">Упражнение</a> </h6> </div> <div id="collapsible-control-right-group0" class="panel-collapse collapse"> <div class="panel-body"><div class="col-md-6"><div class="col-md-12"> <div class="form-group"> <label>Выберете упражнение</label> <input type="text" placeholder="начните вводить название" class="form-control" list="exercise_search" required name="simple_exercise_id[]" autocomplete="off"> <datalist id="exercise_search"> @foreach($exercises as $exercise) <option label="{{$exercise->name}}"  value="{{$exercise->id}}"></option> @endforeach </datalist> </div> </div> <!-- Live search support --> <div class="col-md-12"> <div class="form-group"> <label>Количество подходов:</label> <input type="text"  class="form-control" required name="attempt_numb[]"> </div> </div> <div class="col-md-12"> <div class="form-group"> <label>Вес:</label> <input type="text"  class="form-control" required name="weight[]"> </div> </div> </div><div class="col-md-6"><div class="col-md-12"> <div class="form-group"> <label>Кол-во повторений за подход:</label> <input type="text" class="form-control" required name="repeat_numb[]"> </div> </div> <div class="col-md-12"> <div class="form-group"> <label>Отдых между подходами:</label> <input type="text"  class="form-control" required name="relax_b_repeat[]"> </div> </div> <div class="col-md-12"> <div class="form-group"> <label>Отдых между упражнениями:</label> <input type="text"  class="form-control" required name="relax_b_exercise[]"> </div> </div></div> </div> </div> </div> </div> </div> </div>

  @stop