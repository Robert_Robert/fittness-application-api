@extends('dashboard.layout')




@section('main-content')
<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_layouts.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/components_thumbnails.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/media/fancybox.min.js') }}"></script>

 @if (count($errors) > 0)
   @foreach ($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
      {{ $error }}
    </div>
   @endforeach 
 @endif 

     
  <!-- Main content -->
   

        <!-- Page header -->
        
         
        <div class="page-header">
                  
        </div>

          <!-- Content area -->
        <div class="content">
        <input type="hidden" name="_method" value="DELETE" >
        <form class="form-horizontal" action="{{url('/edit-exercise/'.$exercise->id)}}" method="POST" enctype="multipart/form-data">
           {{ csrf_field() }}
           <input name="_method" type="hidden" value="PUT">

            <div class="panel panel-flat">
              <div class="panel-heading">
                <h5 class="panel-title">Редактировать упражнение</h5>
                <div class="heading-elements">
                  
                        </div>
              </div>

              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                      <fieldset>


                     
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Название:</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" placeholder="название" name="name" value="{{ $exercise->name }}">
                        </div>
                      </div>

                      
                       <div class="form-group">
                        <label class="col-lg-3 control-label">Описание:</label>
                        <div class="col-lg-9">
                          <textarea rows="5" cols="5" class="form-control" placeholder="описание" name="description">{{ $exercise->description }}</textarea>
                        </div>
                      </div>

                       <div class="form-group">
                        <label class="col-lg-3 control-label" >Картинка превью:</label>
                        <div class="col-lg-9" style="width: 55%;">
                
                          <div class="panel panel-flat">
                           

                            <div class="panel-body">
                            
                              <div class="thumbnail">
                                <div class="thumb" style="width: 100%; height: 200px;">
                                  <a href="{{ asset($exercise->image) }}" data-popup="lightbox" >
                                    <img src="{{ asset($exercise->image) }}" alt="" style="height: 200px;">
                                    <span class="zoom-image"><i class="icon-plus2"></i></span>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                             
                        </div>
                      </div>

                        <div class="form-group">
                        <label class="col-lg-3 control-label" style="margin-top: -10px;">Сменить картинку:</label>
                        <div class="col-lg-9" >
                          <div class="uploader bg-warning"><input type="file" class="file-styled" name="image"><span class="action" style="-webkit-user-select: none;"><i class="icon-googleplus5"></i></span></div>
                        </div>
                      </div>

                     <div class="form-group">
                        <label class="col-lg-3 control-label">Група мышц:</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" placeholder="Група мышц" list="mouscle_group" name="muscle_group" value="{{ $exercise->muscle_group }}">
                            <datalist id="mouscle_group">
                                @foreach($groups as $group)
                                    <option label="{{$group->muscle_group}}"  value="{{$group->muscle_group}}"></option>
                                @endforeach
                            </datalist>
                        </div>
                      </div>
    

                     
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Тип упражнения:</label>
                        <div class="col-lg-9">
                          <select data-placeholder="выберете тип упражнения" class="select" name="exercise_type">
                            <option></option>
                              <option value="на время" @if($exercise->exercise_type == 'на время') selected @endif>На время</option>
                              <option value="повторения" @if($exercise->exercise_type == 'повторения') selected @endif>Повторения</option>
                          </select>
                        </div>
                      </div>



                          <div class="form-group">
                              <label class="col-lg-3 control-label">Пол</label>
                              <div class="col-lg-9">
                                  <select data-placeholder="Выберете пол" class="select" name="sex">
                                      <option></option>
                                      <option value="male" @if($exercise->sex == 'male') selected @endif>Мужской</option>
                                      <option value="female" @if($exercise->sex == 'female') selected @endif>Женский</option>
                                  </select>
                              </div>
                          </div>



                          <div class="form-group">
                              <label class="col-lg-3 control-label">Место</label>
                              <div class="col-lg-9">
                                  <select data-placeholder="Выберете место тренировки" class="select" name="place">
                                      <option></option>
                                      <option value="home" @if($exercise->place == 'home') selected @endif>Дом</option>
                                      <option value="gym" @if($exercise->place == 'gym') selected @endif>Зал</option>
                                  </select>
                              </div>
                          </div>


                     <div class="form-group">
                        <label class="col-lg-3 control-label" >Вес:</label>
                        <div class="col-lg-9">
                          <div class="row">
                           <div class="col-md-6">
                              <input type="text" placeholder="вес" class="form-control mb-15" name="weight"  value="{{ $exercise->weight }}">
                          </div>
                        </div>
                      </div>
                     </div>




                      </fieldset>
                  </div>

                  <div class="col-md-6">
                    <fieldset>

                      <div class="form-group">
                          <label class="col-lg-3 control-label"  style="margin-top: -10px;">Вид нагрузки статическая:</label>
                          <div class="col-lg-9">
                            <input type="text" placeholder="" class="form-control" name="load_type" value="{{ $exercise->load_type }}">
                          </div>
                      </div>

                      
                      <div class="form-group">
                        <label class="col-lg-3 control-label" style="margin-top: -10px;">Всегда с травмами:</label>
                        <div class="col-lg-9">
                          <div class="row">
                            <div class="col-md-6">
                              <input type="text" placeholder="да/нет" class="form-control mb-15" name="always_injury" value="{{ $exercise->always_injury }}">
                            </div>
                          </div>
                      </div>
                      </div>
                       

                      <div class="form-group" style="margin-bottom: 8px;">
                        <label class="col-lg-3 control-label" style="margin-top: -10px;">Никогда с травмами:</label>
                        <div class="col-lg-9">
                          <div class="row">
                            <div class="col-md-6">
                              <input type="text" placeholder="да/нет" class="form-control mb-15" name="newer_injury" value="{{ $exercise->newer_injury }}">
                          </div>
                        </div>
                      </div>
                      </div> 
                        
                      
                  


                     <div class="form-group">
                        <label class="col-lg-3 control-label">Видео:</label>
                        <div class="col-lg-9">
                           <div class="panel panel-flat">
                            <div class="panel-body">
                              <div class="thumbnail" >
                                  <div class="thumb" style="width: 100%; height: 200px;">
                                      <video controls="controls" style="height: 200px; width: 100%;" >
                                          <source src="{{asset($exercise->animation)}}" type="video/mp4"  />
                                      </video>
                                  <div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                   </div>


                      <div class="form-group">
                        <label class="col-lg-3 control-label" >Сменить видео:</label>
                        <div class="col-lg-9">
                          <div class="uploader bg-warning"><input type="file" class="file-styled" name="animation"><span class="action" style="-webkit-user-select: none;"><i class="icon-googleplus5"></i></span></div>
                        </div>
                      </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" style="margin-top: -10px;">Скрытое упражнение:</label>
                            <div class="col-lg-9">
                                <input type="checkbox" class="switchery-info" style="height: 20px; width: 20px;" name="hidden_exercise" value="1" @if($exercise->hidden_exercise == 1) checked @endif >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Сложность упражнения</label>
                            <div class="col-lg-9">
                                <select data-placeholder="Выберете сложность" class="select" name="exercise_level">
                                    <option></option>
                                    <option value="1" @if($exercise->exercise_level == '1') selected @endif>1</option>
                                    <option value="2" @if($exercise->exercise_level == '2') selected @endif>2</option>
                                    <option value="3" @if($exercise->exercise_level == '3') selected @endif>3</option>
                                    <option value="4" @if($exercise->exercise_level == '4') selected @endif>4</option>
                                    <option value="5" @if($exercise->exercise_level == '5') selected @endif>5</option>
                                    <option value="6" @if($exercise->exercise_level == '6') selected @endif>6</option>
                                    <option value="7" @if($exercise->exercise_level == '7') selected @endif>7</option>
                                    <option value="8" @if($exercise->exercise_level == '8') selected @endif>8</option>
                                    <option value="9" @if($exercise->exercise_level == '9') selected @endif>9</option>
                                    <option value="10" @if($exercise->exercise_level == '10') selected @endif>10</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label">Уровень пользователя</label>
                            <div class="col-lg-9">
                                <select data-placeholder="Выберете уровень пользователя" class="select" name="user_level">
                                    <option></option>
                                    <option value="easy" @if($exercise->user_level == 'easy') selected @endif>Новичок</option>
                                    <option value="medium" @if($exercise->user_level == 'medium') selected @endif>Средний</option>
                                    <option value="hard" @if($exercise->user_level == 'hard') selected @endif>Опытный</option>
                                </select>
                            </div>
                        </div>


                      <div class="form-group">
                        <label class="col-lg-3 control-label">Аксесуары:</label>
                        <div class="col-lg-9" id="acsesuar_area">
                         
                            
                            @foreach($accessories as $accessory)
                            <div style="margin-bottom: 10px;" class="accessory_parent">
                              <input type="text" class="form-control" placeholder="Teal button"   value="{{$accessory->accessory}}" style="width: 78%; display: inline-block;">
                                <button class="btn bg-teal del-accessory" type="button" data-accessory="{{$accessory->id}}">Удалить</button>
                            </div>  
                            @endforeach
                              <input type="text" placeholder="аксесуар" class="form-control" name="accessory[]" style="margin-top: 10px; width: 88%; display: inline-block;">
                               <button type="button" class="btn bg-pink flex-item " style="float: right; margin-top: 10px;" id="add_acsesuar"><i class="icon-googleplus5"></i></button>
                            </div>

                           
                          
                           

                     
                      </div>


                    </fieldset>
                  </div>
                </div>

                <div class="text-right">
                  <button type="submit" class="btn btn-primary">Изменить<i class="icon-arrow-right14 position-right"></i></button>
                </div>
              </div>
            </div>
          </form>
          </div>

   <!-- Theme JS files -->
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_layouts.js') }}"></script>
 <!--  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/pages/form_checkboxes_radios.js') }}"></script>
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/styling/switch.min.js') }}"></script> -->
  <!-- /theme JS files -->






          <!-- Footer -->
          <div class="footer text-muted">
            &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
          </div>
          <!-- /footer -->

        </div>
        <!-- /content area -->    
        
      
@stop