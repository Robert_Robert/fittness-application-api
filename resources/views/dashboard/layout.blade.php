<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>FitnessApp</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('resources/assets/admin/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('resources/assets/admin/assets/css/minified/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('resources/assets/admin/assets/css/minified/core.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('resources/assets/admin/assets/css/minified/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('resources/assets/admin/assets/css/minified/colors.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('resources/assets/admin/assets/css/additional.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->



	<!-- multiselect -->
    <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<!-- <script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/plugins/notifications/pnotify.min.js') }}"></script> -->


	<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/core/app.js') }}"></script>
	

	<!-- /theme JS files -->



	<script type="text/javascript" src="{{ asset('resources/assets/admin/assets/js/main.js') }}"></script>
</head>

<body class="pace-done"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset('resources/assets/admin/assets/images/fitness.jpg') }}" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			

			
			
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container" style="min-height:266px">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
						
							

								
								<li><a href="{{url('/exercise-list')}}"><i class="icon-list-unordered"></i> <span>Упражнения</span></a></li>

								<li><a href="{{url('/program-list')}}"><i class="icon-stack2"></i> <span>Програмы</span></a></li>

								<li><a href="{{url('/new-admin')}}"><i class="icon-stack2"></i> <span>Добавить администратора</span></a></li>

								<li><a href="{{url('/admin-logout')}}"><i class="icon-stack2"></i> <span>Выйти</span></a></li>

								<!-- /main -->

								

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
			   
			<div class="content-wrapper">

				@yield('main-content')

			</div>
 
		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->





</body>


</html>
