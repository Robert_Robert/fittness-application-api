$('document').ready(function(){

var counter = 1;

	$('#add_acsesuar').click(function(ev){
		$('#acsesuar_area').append('<input type="text" placeholder="аксесуар" class="form-control" name="accessory[]" style="margin-top: 10px;">');
	});

	$('#add_train_day').click(function(ev){
		$('#train_days_block').append('');
	});

	$('.add-exercise').click(function(ev){

		var exercise =$('.new-exercise:first');
		var txt = exercise.html();
		var new_exercise = txt.replace('#collapsible-control-right-group0', '#collapsible-control-right-group'+counter.toString()).replace('collapsible-control-right-group0', 'collapsible-control-right-group'+counter.toString()).replace('btn-group bootstrap-select', 'btn-group bootstrap-select  dropup');

		//var exercise = $('.new-exercise:first').html();
		$(this).closest('.panel').find('.exercise-container').prepend(new_exercise);
		
		counter++;
	});

	

	$('.del-accessory').click(function(ev){
	

		var model = $(this).attr('data-accessory'), it = $(this);
		

		$.ajax({
	 	url: '/del-accessory/'+model,
	 	type: "DELETE",
	 	data :({'_token': $('input[name=_token]').val(), '_method': $('input[name=_method]').val() }),
	 	dataType: "html",

	 	success: function(data, d){
	 			 it.closest('.accessory_parent').remove();
	 		}
	 });



	});	

	$('.delete-exercise').click(function(ev){
	

		var model = $(this).attr('data-exercise'), it = $(this);
		

		$.ajax({
	 	url: '/del-exercise/'+model,
	 	type: "DELETE",
	 	data :({'_token': $('input[name=_token]').val(), '_method': $('input[name=_method]').val() }),
	 	dataType: "html",

	 	success: function(data, d){
	 			 it.closest('tr').remove();

	 		}
	 	});

	});	

	$('.delete-day').click(function(ev){
		var model = $(this).attr('data-day'), it = $(this);


		$.ajax({
	 	url: '/del-day/'+model,
	 	type: "DELETE",
	 	data :({'_token': $('input[name=_token]').val(), '_method': $('input[name=_method]').val() }),
	 	dataType: "html",

	 	success: function(data, d){
	 			 it.closest('.program-day').remove();

	 		}
	 	});

	});

	$('#imt').change(function(e){
		var val = this.value;
		switch (val){
			case '15-21.9':
				$('#imt_type').html('<option value="ИМТ 1" selected>ИМТ 1</option>');
				$('#imt_type_value').val('ИМТ 1');
				break;
			case '22-26':
				$('#imt_type').html('<option value="ИМТ 2" selected>ИМТ 2</option>');
				$('#imt_type_value').val('ИМТ 2');
				break;
			case '27-30':
				$('#imt_type').html('<option value="ИМТ 3" selected>ИМТ 3</option>');
				$('#imt_type_value').val('ИМТ 3');
				break;
			case '31-100':
				$('#imt_type').html('<option value="ИМТ 4" selected>ИМТ 4</option>');
				$('#imt_type_value').val('ИМТ 4');
				break;
		}
	});

	


});