<?php

return array(

    'fitnesAppIOS'     => array(
        'environment' =>'development',
        'certificate' =>app_path().'/apns-pro.pem',
        'passPhrase'  =>"food",
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'yourAPIKey',
        'service'     =>'gcm'
    )

);